\documentclass[12pt,twoside,a4paper]{article}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
%\usepackage[makeroom]{cancel}
\usepackage{cancel}
\usepackage{color}
\usepackage{fancyhdr}
\usepackage[a4paper,left=2cm,right=2cm]{geometry}
\usepackage{graphicx}
\usepackage[colorlinks]{hyperref}
\usepackage{ifthen}
\usepackage[utf8]{inputenc}
\usepackage{lastpage}
\usepackage{physics}
\usepackage{relsize}
\usepackage{simplewick}
\usepackage{subcaption}
\usepackage{tikz}
\usetikzlibrary{patterns}

%Biblatex
\usepackage[style=numeric,sorting=nyt,isbn=true,backend=biber]{biblatex}
\addbibresource{HDSol.bib}

% Define some commands I use regularly
\newcommand*{\lr}[1]{\left( {#1} \right)}
\newcommand*{\lrb}[1]{\left[ {#1} \right]}
\newcommand*{\lrc}[1]{\left\{ {#1} \right\}}
\newcommand*{\e}{\operatorname{e}}
\newcommand*{\im}{\operatorname{i}}
\newcommand*{\qop}[1]{\mathrm{\widehat{#1}}}
\newcommand*{\diracdelta}[2][]{\, \delta \ifthenelse{\equal{#1}{}}{}{^{\left({#1}\right)}} \left( {#2} \right)}
\newcommand*{\dx}[1][]{\dd[#1]{x}}
\newcommand*{\pathint}[1][]{\int \mathcal{D}{#1}\,}
\DeclareMathOperator{\cosec}{cosec}
% Blackboard 1 = \mathbbold{1}
\newcommand{\bbfamily}{\fontencoding{U}\fontfamily{bbold}\selectfont}
\DeclareMathAlphabet{\mathbbold}{U}{bbold}{m}{n}

% style allowing styles to be applied to each segment of a path
% http://tex.stackexchange.com/questions/3161/tikz-how-to-draw-an-arrow-in-the-middle-of-the-line
\usetikzlibrary{decorations.pathreplacing,decorations.markings}
\tikzset{
  on each segment/.style={
    decorate,
    decoration={
      show path construction,
      moveto code={},
      lineto code={
        \path [#1]
        (\tikzinputsegmentfirst) -- (\tikzinputsegmentlast);
      },
      curveto code={
        \path [#1] (\tikzinputsegmentfirst)
        .. controls
        (\tikzinputsegmentsupporta) and (\tikzinputsegmentsupportb)
        ..
        (\tikzinputsegmentlast);
      },
      closepath code={
        \path [#1]
        (\tikzinputsegmentfirst) -- (\tikzinputsegmentlast);
      },
    },
  },
  % style to add an arrow in the middle of a path
  mid arrow/.style={postaction={decorate,decoration={
        markings,
        mark=at position .5 with {\arrow[#1]{stealth}}
      }}},
}

%Now define values for my document layout and header
\pagestyle{fancy}
\setlength{\voffset}{-0.5in}
\addtolength{\textheight}{89pt}
\setlength{\headheight}{33pt}
\parindent 0pt
\parskip 12pt
\title{PHYS11012 Hamiltonian Dynamics}
\lfoot{PHYS11012 Hamiltonian Dynamics}
\author{Example Sheet 2 Solutions}
\rfoot{Example Sheet 2 Solutions}
\date{Fri 29$^{\mathrm{th}}$ Jan 2021}
\cfoot{\thepage{} of \pageref{LastPage}}
\lhead{\includegraphics[scale=0.4]{crest.pdf}}

\begin{document}
\thispagestyle{empty}
\maketitle
\begin{center}
\includegraphics{crest.pdf}\\
\end{center}

% Section headings: Question n
\def\thesection{Q\arabic{section}}
%\def\thesection{\arabic{section}}
\def\thesubsection{\arabic{section}.\alph{subsection}}
\def\thesubsubsection{\arabic{section}.\alph{subsection}.\roman{subsubsection}}

Latest version: \href{https://git.ecdf.ed.ac.uk/s1786208/HD21}{https://git.ecdf.ed.ac.uk/s1786208/HD21}

% Table of contents
\tableofcontents

\newpage
%1
\section{}


We are told that a small rotation, $\delta \theta$, about $\vec{n}$ changes $\vec{r}$ by $\delta r$, i.e.:
\begin{align}
\label{eq:q1_1} \delta \vec{r} &= \vec{n} \times \vec{r} \, \delta \theta
\intertext{Without loss of generality, we restrict ourselves to a two-dimensional, $\theta$-$t$ configuration space (i.e. coordinate system, see notes, pg 3).}
\intertext{The position of the $k$-th particle depends on the $k$-th generalised position coordinate and time, while its velocity also depends on the $k$-th generalised velocity:}
\vec{r}_k \lr{\theta_k, t} &\equiv \textrm{Position of the $k$-th particle}\\
\dot{\vec{r}}_k \lr{\theta_k, \dot{\theta}_k, t} &\equiv \textrm{Velocity of the $k$-th particle}
\intertext{For notational convenience, we define collections of generalised co-ordinates and velocities:}
\vec{\theta} &= \lr{\theta_1, \theta_2, \ldots, \theta_k}\\
\vec{\dot{\theta}} &= \lr{\dot{\theta_1}, \dot{\theta}_2, \ldots, \dot{\theta}_k}
\intertext{Our Lagrangian, $L$, is the difference between kinetic energy, $T$, and potential, $V$:}
L \lr{\vec{\theta}, \dot{\vec{\theta}}, t} &= T \lr{\vec{\theta}, \dot{\vec{\theta}}, t} - V \lr{\vec{\theta}, \dot{\vec{\theta}}, t}
\intertext{from which, momentum in the $\theta$-direction is defined to be:}
\label{eq:q1_2} p_\theta &= \pdv{L}{\dot{\theta}} = \pdv{T}{\dot{\theta}} - \pdv{V}{\dot{\theta}}
\intertext{The definition of the dot product (see apdx~\ref{apdx:dotprod}) allows us to say that in any coordinate system:}
T \lr{\vec{\theta}, \dot{\vec{\theta}}, t} &= \frac{1}{2} \sum_k m_k \dot{\vec{r}}_k^2
\intertext{so that:}
\pdv{T}{\dot{\theta}} &= \pdv{\dot{\theta}} \lr{\frac{1}{2} \sum_k m_k \dot{\vec{r}}_k^2}\\
&= \sum_k m_k \dot{\vec{r}}_k \cdot \pdv{\vec{\dot{r}}_k}{\dot{\theta}}
\intertext{we cancel the dots (see example sheet 1, question 3) obtaining:}
\label{eq:q1_3} \pdv{T}{\dot{\theta}} &= \sum_k m_k \dot{\vec{r}}_k \cdot \pdv{\vec{r}_k}{\theta}
\intertext{From \eqref{eq:q1_1} we can say:}
\label{eq:q1_4} \pdv{\vec{r}_k}{\theta} &=  \vec{n} \times \vec{r}_k
\intertext{so \eqref{eq:q1_3} becomes:}
\pdv{T}{\dot{\theta}} &= \sum_k m_k \dot{\vec{r}}_k \cdot \lr{\vec{n} \times \vec{r}_k}
\intertext{Applying the scalar triple product, $a \cdot \lr{b \times c} = b \cdot \lr{c \times a} = \lr{a \times b} \cdot c$}
\pdv{T}{\dot{\theta}} &= \vec{n} \cdot \sum_k m_k \lr{\vec{r}_k \times \dot{\vec{r}}_k}
\intertext{Recognising the sum as the total angular momentum, $\vec{L}$, of our system:}
\vec{L} &= \sum_k m_k \lr{\vec{r}_k \times \dot{\vec{r}}_k}
\intertext{we have:}
\pdv{T}{\dot{\theta}} &= \vec{n} \cdot \vec{L}
\intertext{i.e. from \eqref{eq:q1_2}:}
\label{eq:q1_5} p_\theta &= \vec{n} \cdot \vec{L} - \pdv{V}{\dot{\theta}}
\end{align}

\subsection{$V$ independent of $\dot{\theta}$}

If $V$ is independent of $\dot{\theta}$ then:
\begin{align}
\pdv{V}{\dot{\theta}} &= 0
\intertext{so from \eqref{eq:q1_5} we have our result immediately, i.e.:}
p_\theta &= \vec{n} \cdot \vec{L}
\end{align}
which is the component of angular momentum about the axis $\vec{n}$.

\subsection{$V$ depends on $\dot{\theta}$}

When our potential is velocity dependent, applying the chain rule to \eqref{eq:q1_5} gives:
\begin{align}
p_\theta &= \vec{n} \cdot \vec{L} - \sum_k \pdv{V}{\vec{\dot{r}}_k} \cdot \pdv{\vec{\dot{r}}_k}{\dot{\theta}}
\intertext{cancelling the dots and using \eqref{eq:q1_4} again:}
p_\theta &= \vec{n} \cdot \vec{L} - \sum_k \pdv{V}{\vec{\dot{r}}_k} \cdot \lr{\vec{n} \times \vec{r}_k}
\intertext{which is more simply expressed (scalar triple product again):}
\label{eq:q1b1} p_\theta &= \vec{n} \cdot \lr{\vec{L} - \sum_k \vec{r}_k \times \pdv{V}{\vec{\dot{r}}_k}}
\end{align}

\subsection{Charged particle in electromagnetic field}

The potential for a charged particle (using the Lagrangian given in tutorial 1, question 4) is:
\begin{align}
V \lr{\vec{r}, \dot{\vec{r}}} = V \lr{\theta, \dot{\theta}, t} &= q \lr{\varphi \lr{\vec{r}} - \frac{\dot{\vec{r}}}{c} \cdot \vec{A} \lr{\vec{r}}}
\intertext{Noting that $\varphi$ and $\vec{A}$ are independent of $\vec{\dot{r}}$:}
\pdv{V}{\vec{\dot{r}}} &= - \frac{q}{c} \vec{A}
\intertext{using (\ref{eq:q1b1}) with $N_k=1$ immediately gives our single-particle result:}
p_\theta &= \vec{n} \cdot \lr{\vec{L} + \frac{q}{c} \, \vec{r} \times \vec{A}}
\intertext{i.e. our extra term is of the form:}
&\quad \frac{q}{c} \, \vec{n} \cdot \lr{\vec{r} \times \vec{A}}
\end{align}

%2
\section{}

We are given the Lagrangian for a simple harmonic oscillator:
\begin{align}
\label{eq:q2_1} L \lr{x, \dot{x}} &= \frac{1}{2} m \dot{x}^2 - \frac{1}{2} m \omega^2 x^2
\end{align}

\subsection{$L$ invariant up to a gauge transformation}

Under the transformation:
\begin{align}
\label{eq:q2a1} x \longmapsto x' &= x + A \sin \omega t + B \cos \omega t\\
\intertext{we differentiate to find:}
\dot{x}' &= \dot{x} + \omega \lr{A \cos \omega t - B \sin \omega t}
\intertext{Substituting $x'$ and $\dot{x}'$ into \eqref{eq:q2_1}, we see the Lagrangian transforms as:}
L \longmapsto L' \lr{x, \dot{x}} &= \frac{1}{2} m \lr{\dot{x} + \omega \lr{A \cos \omega t - B \sin \omega t}}^2 - \frac{1}{2} m \omega^2 \lr{x + A \sin \omega t + B \cos \omega t}^2\\
&= \frac{1}{2} m \dot{x}^2 - \frac{1}{2} m \omega^2 x^2\\
&\qquad + \dot{x} m \omega \lr{A \cos \omega t - B \sin \omega t} - x m \omega^2 \lr{A \sin \omega t + B \cos \omega t}\\
&\qquad + \frac{1}{2} m \omega^2 \lr{A \cos \omega t - B \sin \omega t}^2 - \frac{1}{2} m \omega^2 \lr{A \sin \omega t + B \cos \omega t}^2\\
&= L \lr{x, \dot{x}} + \dv{t} \lr{m \omega x \lr{A \cos \omega t - B \sin \omega t}}\\
&\qquad + \frac{1}{2} m \omega^2 \Big(A^2 \cos^2 \omega t + B^2 \sin^2 \omega t - 2 A B \cos \omega t \sin \omega t\\
&\qquad \qquad - A^2 \sin^2 \omega t - B^2 \cos^2 \omega t - 2 A B \sin \omega t \cos \omega t \Big)
\intertext{using $\cos 2a = \cos^2 a - \sin^2 a$ and $\sin 2 a = 2 \sin a \cos a$:}
L' \lr{x, \dot{x}} &= L \lr{x, \dot{x}} + \dv{t} \lr{m \omega x \lr{A \cos \omega t - B \sin \omega t}}\\
&\qquad + \frac{1}{2} m \omega^2 \lr{ \lr{A^2 - B^2} \cos 2 \omega t - 2 A B \sin 2 \omega t}
\intertext{i.e.:}
L' \lr{x, \dot{x}} &= L \lr{x, \dot{x}} + \dv{F \lr{x, t}}{t}
\intertext{where:}
\label{eq:q2a2} F \lr{x, t} &= m \omega x \lr{A \cos \omega t - B \sin \omega t} + \frac{1}{2} m \omega \lr{ \frac{1}{2} \lr{A^2 - B^2} \sin 2 \omega t + A B \cos 2 \omega t}
\end{align}
i.e. the Lagrangian is invariant up to a gauge transformation.

\subsection{Constants of motion}

Noether's theorem (see notes pp 7-8): if we have $r$, independent, $\epsilon \rightarrow 0$ transformations:
\begin{align}
q \longmapsto Q &= q + \epsilon \, w^{\lr{r}}
\intertext{where $L$ is invariant up to an associated gauge transformation, i.e.:}
L \lr{\vec{q}, \dot{\vec{q}}, t} \longmapsto L' \lr{\vec{Q}, \dot{\vec{Q}}, t} &= L \lr{\vec{q}, \dot{\vec{q}}, t} - \epsilon \dv{G^{\lr{r}}}{t} + \order{\epsilon^2}
\intertext{then we have $r$ conserved quantities:}
\label{eq:q2b1} I^{\lr{r}} \equiv \pdv{L}{\dot{q}} w^{\lr{r}} + G^{\lr{r}} &= \textrm{const}
\end{align}

\subsubsection{$A = \epsilon \rightarrow 0$, $B=0$}

From (\ref{eq:q2a1}) and (\ref{eq:q2a2}) (noting that $A^2$ is $\order{\epsilon^2}$):
\begin{align}
w^{\lr{A}} &= \sin \omega t\\
G^{\lr{A}} &= -F^{\lr{A}} = - m \omega x \cos \omega t
\intertext{so from (\ref{eq:q2b1}) the constant of motion associated with an infinitesimal transformation $A$ is:}
I^{\lr{A}} &= m \dot{x} \sin \omega t - m \omega x \cos \omega t
\intertext{We determine $I^{\lr{A}}$ by substituting $t=0$, $x=x_0$, i.e.:}
I^{\lr{A}} &= -m \omega x_0\\
\intertext{so:}
\label{eq:q2bIA} - \omega x_0 &= \dot{x} \sin \omega t - \omega x \cos \omega t
\end{align}

\subsubsection{$A=0$, $B = \epsilon \rightarrow 0$}

From (\ref{eq:q2a1}) and (\ref{eq:q2a2}) (noting that $B^2$ is $\order{\epsilon^2}$):
\begin{align}
w^{\lr{B}} &= \cos \omega t\\
G^{\lr{B}} &= -F^{\lr{B}} = m \omega x \sin \omega t
\intertext{so from (\ref{eq:q2b1}) the constant of motion associated with an infinitesimal transformation $B$ is:}
I^{\lr{B}} &= m \dot{x} \cos \omega t + m \omega x \sin \omega t
\intertext{We determine $I^{\lr{A}}$ by substituting $t=0$, $\dot{x} = v_0$, i.e.:}
I^{\lr{B}} &= m v_0
\intertext{so:}
\label{eq:q2bIB} v_0 &= \dot{x} \cos \omega t + \omega x \sin \omega t
\end{align}

\subsection{General solution to equation of motion}

(\ref{eq:q2bIA}) $\lr{- \cos \omega t} \, + $ (\ref{eq:q2bIB}) $\sin \omega t$ gives:
\begin{align}
\omega x_0 \cos \omega t + v_0 \sin \omega t &= \lr{\dot{x} \sin \omega t - \omega x \cos \omega t} \lr{- \cos \omega t} + \lr{\dot{x} \cos \omega t + \omega x \sin \omega t} \sin \omega t\\
&= \omega x \lr{\cos^2 \omega t + \sin^2 \omega t}\\
\label{eq:q2c1} x \lr{t} &= x_0 \cos \omega t + \frac{v_0}{\omega} \sin \omega t
\intertext{Similarly (though not asked for) (\ref{eq:q2bIA}) $\sin \omega t \, + $ (\ref{eq:q2bIB}) $\cos \omega t$ gives:}
- \omega x_0 \sin \omega t + v_0 \cos \omega t &= \lr{\dot{x} \sin \omega t - \omega x \cos \omega t} \sin \omega t + \lr{\dot{x} \cos \omega t + \omega x \sin \omega t} \cos \omega t\\
&= \dot{x} \lr{\sin^2 \omega t + \cos^2 \omega t}\\
\dot{x} \lr{t} &= v_0 \cos \omega t - \omega x_0 \sin \omega t
\end{align}
which is the same as we obtain by differentiating (\ref{eq:q2c1}) directly.

%3
\section{}

We are given the potential energy of a particle of mass, $m$, (cylindrical co-ords; $V_0$, $\rho_0$ const):
\begin{align}
V \lr{\rho} &= V_0 \ln \frac{\rho}{\rho_0}
\end{align}

We find the Lagrangian (as a function of generalised co-ordinates and generalised velocities):
\begin{align}
\label{eq:q31} L \lr{\rho, \varphi, z, \dot{\rho}, \dot{\varphi}, \dot{z}} = T - V = \frac{1}{2} m \dot{\vec{r}}^2 - V \lr{\rho}
\end{align}
proceeding in either of two equivalent ways.

\subsubsection*{i) Cartesian coordinates}

Thinking of $\vec{r}$ as a Cartesian vector valued function of cylindrical co-ordinates:
\begin{align}
\vec{r} \lr{\rho, \varphi, z} &= \lr{\rho \cos \varphi, \rho \sin \varphi, z}
\intertext{differentiating to get the velocity:}
\dot{\vec{r}} &= \lr{\dot{\rho} \cos \varphi - \rho \dot{\varphi} \sin \varphi, \dot{\rho} \sin \varphi + \rho \dot{\varphi} \cos \varphi, \dot{z}}
\intertext{we substitute into (\ref{eq:q31}) to obtain the Lagrangian:}
L &= \frac{1}{2} m \Big(\dot{\rho}^2 \cos^2 \varphi + \rho^2 \dot{\varphi}^2 \sin^2 \varphi - 2 \rho \dot{\rho} \dot{\varphi} \cos \varphi \sin \varphi\\
&\qquad + \dot{\rho}^2 \sin^2 \varphi + \rho^2 \dot{\varphi}^2 \cos^2 \varphi + 2 \rho \dot{\rho} \dot{\varphi} \cos \varphi \sin \varphi\\
&\qquad + \dot{z}^2\Big) - V_0 \ln \frac{\rho}{\rho_0}
\intertext{i.e. the Lagrangian (as a function of generalised co-ordinates and velocities) is:}
\label{eq:q3L} L \lr{\rho, \dot{\rho}, \dot{\varphi}, \dot{z}} &= \frac{1}{2} m \lr{\dot{\rho}^2 + \rho^2 \dot{\varphi}^2 + \dot{z}^2} - V_0 \ln \frac{\rho}{\rho_0}
\end{align}

\subsubsection*{ii) Cylindrical coordinates}

Thinking of $\vec{r}$ as a vector in cylindrical co-ordinates, we calculate the metric using \eqref{eq:apdx_metric}:
\begin{align}
\label{eq:q3m1} g_{11} &= \lr{\pdv{x}{\rho}}^2 + \lr{\pdv{y}{\rho}}^2 + \lr{\pdv{z}{\rho}}^2 = \cos^2 \varphi + \sin^2 \varphi = 1\\
\label{eq:q3m2} g_{22} &= \lr{\pdv{x}{\varphi}}^2 + \lr{\pdv{y}{\varphi}}^2 + \lr{\pdv{z}{\varphi}}^2 = \rho^2 \sin^2 \varphi +  \rho^2 \cos^2 \varphi = \rho^2\\
\label{eq:q3m3} g_{33} &= \lr{\pdv{x}{z}}^2 + \lr{\pdv{y}{z}}^2 + \lr{\pdv{z}{z}}^2 = 1
\end{align}
all other components of the metric are zero, because cylindrical co-ordinates are orthogonal.

Using the metric to evaluate $\dot{\vec{r}}^2 = \sum_{i, j} g_{ij} \dot{r}_i \dot{r}_j$ in (\ref{eq:q31}) we obtain our result immediately:
\begin{align}
\label{eq:q3L2} L \lr{\rho, \dot{\rho}, \dot{\varphi}, \dot{z}} &= \frac{1}{2} m \lr{\dot{\rho}^2 + \rho^2 \dot{\varphi}^2 + \dot{z}^2} - V_0 \ln \frac{\rho}{\rho_0}
\end{align}

\subsubsection*{iii) Both approaches give the same result}

Regardless of how we defined our position vector, the Lagrangian is defined in terms of generalised co-ordinates (which in this example happen to be cylindrical co-ordinates), so we obtain the same answer either way, i.e. \eqref{eq:q3L} and \eqref{eq:q3L2} agree.

\subsection{Hamiltonian}

To obtain the Hamiltonian, we use the Legendre transform:
\begin{align}
\label{eq:Legendre} H \lr{\rho, \varphi, z, p_\rho, p_\varphi, p_z} &= \sum_i p_i \dot{q}_i - L
\intertext{making sure to express all variables in terms of generalised coordinates and conjugate momenta.}
\intertext{From the definition of conjugate momenta and \eqref{eq:q3L2}:}
\label{eq:q3a1} p_\rho &= \pdv{L}{\dot{\rho}} = m \dot{\rho} & \dot{\rho} &= \frac{p_\rho}{m}\\
\label{eq:q3a2} p_\varphi &= \pdv{L}{\dot{\varphi}} = m \rho^2 \dot{\varphi} & \dot{\varphi} &= \frac{p_\varphi}{m \rho^2}\\
\label{eq:q3a3} p_z &= \pdv{L}{\dot{z}} = m \dot{z} & \dot{z} &= \frac{p_z}{m}
\end{align}
and using the Legendre transform, $H = \sum_i p_i \dot{q}_i - L$, we obtain the Hamiltonian:
\begin{align}
H &= \frac{p_\rho^2}{m} + \frac{p_\varphi^2}{m \rho^2} + \frac{p_z^2}{m} - \lr{\frac{1}{2} m \lr{\lr{\frac{p_\rho}{m}}^2 + \rho^2 \lr{\frac{p_\varphi}{m \rho^2}}^2 + \lr{\frac{p_z}{m}}^2} - V_0 \ln \frac{\rho}{\rho_0}}
\end{align}
i.e. the Hamiltonian (as a function of generalised co-ordinates and conjugate momenta) is:
\begin{align}
\label{eq:q3a4} H \lr{\rho, p_\rho, p_\varphi, p_z} &= \frac{1}{2 m} \lr{p_\rho^2 + \frac{p_\varphi^2}{\rho^2} + p_z^2} + V_0 \ln \frac{\rho}{\rho_0}
\end{align}

\subsection{Hamiltonian equations of motion}

Applying the Hamiltonian equations of motion to (\ref{eq:q3a4}):
\begin{align}
\dot{\rho} &= \pdv{H}{p_\rho} = \frac{p_\rho}{m}\\
\dot{\varphi} &= \pdv{H}{p_\varphi} = \frac{p_\varphi}{m \rho^2}\\
\dot{z} &= \pdv{H}{p_z} = \frac{p_z}{m}\\
\dot{p}_\rho &= - \pdv{H}{\rho} = \frac{p_\varphi^2}{m \rho^3} - \frac{V_0}{\rho}\\
\dot{p}_\varphi &= - \pdv{H}{\varphi} = 0\\
\dot{p}_z &= - \pdv{H}{z} = 0\\
\label{eq:q3b1} &\qquad \, \pdv{H}{t} = 0
\end{align}

\subsection{Conserved quantities}

The Hamiltonian (\ref{eq:q3a4}) is independent of $\varphi$, $z$ and $t$, so the three conserved quantities are:
\begin{align}
(\ref{eq:q3a2}) \implies p_\varphi &= m \rho^2 \dot{\varphi} = \textrm{const} & \textrm{angular momentum}\\
(\ref{eq:q3a3}) \implies p_z &= m \dot{z} = \textrm{const} & \textrm{(axial) linear momentum}\\
(\ref{eq:q3b1}) \implies H &= E = \textrm{const} & \textrm{energy}
\end{align}

%4
\section{}

\subsection{Simple harmonic oscillator}

Here, the potential, $V$ is given by:
\begin{align}
V \lr{x} &= - \int - \kappa x \dd{x} = \frac{1}{2} \kappa x^2
\intertext{so the Lagrangian is:}
L \lr{x, \dot{x}} &= T \lr{\dot{x}} - V \lr{x}\\
&= \frac{1}{2} m \dot{x}^2 - \frac{1}{2} \kappa x^2
\intertext{the conjugate momentum is:}
p &= \pdv{L}{\dot{x}}\\
&= m \dot{x}
\intertext{i.e.:}
\dot{x} &= \frac{p}{m}
\intertext{and the Legendre transform \eqref{eq:Legendre} gives us the Hamiltonian:}
H \lr{x, p} &= p \dot{x} - L\\
&= \frac{p^2}{m} - \lr{\frac{p^2}{2 m} - \frac{1}{2} \kappa x^2}
\intertext{i.e.:}
H \lr{x, p} &= \frac{p^2}{2 m} + \frac{1}{2} \kappa x^2
\intertext{the equations of motion are:}
\dot{x} &= \pdv{H}{p} = \frac{p}{m}\\
\dot{p} &= - \pdv{H}{x} = - \kappa x
\end{align}

\subsection{Particle in a central force-field, rotating axes}

We are given the Lagrangian and told the potential is radially symmetric:
\begin{align}
L &= \frac{1}{2} m \lr{\dot{r}_i + \epsilon_{ijk} \omega_j r_k}^2 - V \lr{\abs{\vec{r}\,}}
\intertext{so:}
p_i &= \pdv{L}{\dot{r}_i}\\
&= m \lr{\dot{r}_i + \epsilon_{ijk} \omega_j r_k}
\intertext{i.e.:}
\dot{r}_i &= \frac{p_i}{m} - \epsilon_{ijk} \omega_j r_k
\intertext{again the Legendre transform \eqref{eq:Legendre} gives us the Hamiltonian:}
H \lr{r_i, p_i} &= p_i \dot{r}_i - L\\
&= p_i \lr{\frac{p_i}{m} - \epsilon_{ijk} \omega_j r_k} - \lr{\frac{1}{2} m \lr{\frac{p_i}{m}} \cdot \lr{\frac{p_i}{m}} - V \lr{\abs{\vec{r}\,}}}
\intertext{i.e. the Hamiltonian is:}
H \lr{\vec{r}, \vec{p}} &= \frac{\vec{p}^2}{2 m} - \vec{p} \cdot \lr{\vec{\omega} \times \vec{r}} + V \lr{\abs{\vec{r}\,}}
\intertext{and the equations of motion are:}
\dot{\vec{r}} &= \pdv{H}{\vec{p}} = \frac{\vec{p}}{m} - \vec{\omega} \times \vec{r}\\
\dot{p}_i &= - \pdv{H}{r_i}\\
\label{eq:q4b1} &= \pdv{r_i} \lr{p_l \epsilon_{ljk} \omega_j r_k - V \lr{\abs{\vec{r}\,}}}\\
&= p_l \epsilon_{lji} \omega_j - V' \lr{\abs{\vec{r}\,}} \pdv{r_i} \lr{\lr{r_j \cdot r_j}^{\flatfrac{1}{2}}}\\
&= \epsilon_{ilj} p_l \omega_j - V' \lr{\abs{\vec{r}\,}} \frac{1}{2} \lr{2 r_i} \lr{r_j \cdot r_j}^{-\flatfrac{1}{2}}
\intertext{i.e.:}
\label{eq:q4b2} \dot{\vec{p}} &= \vec{p} \times \vec{\omega} - V' \lr{\abs{\vec{r}\,}} \frac{\vec{r}}{\abs{\vec{r}\,}}
\end{align}

\subsection{Symmetric, pivoted top}

Given:
\begin{align}
L \lr{\theta, \dot{\theta}, \dot{\phi}, \dot{\psi}} &= \frac{1}{2} A \lr{\dot{\theta}^2 + \dot{\phi}^2 \sin^2 \theta} + \frac{1}{2} C \lr{\dot{\phi} \cos \theta + \dot{\psi}}^2 - m g h \cos \theta
\intertext{the conjugate momenta are:}
\label{eq:q4c1} p_\theta &= \pdv{L}{\dot{\theta}} = A \dot{\theta}\\
\label{eq:q4c2} p_\phi &= \pdv{L}{\dot{\phi}} = A \dot{\phi} \sin^2 \theta + C \lr{\dot{\phi} \cos \theta + \dot{\psi}} \cos \theta\\
\label{eq:q4c3} p_\psi &= \pdv{L}{\dot{\psi}} = C \lr{\dot{\phi} \cos \theta + \dot{\psi}}
\intertext{Rearranging to find generalised velocities in terms of co-ordinates and conjugate momenta:}
(\ref{eq:q4c1}) \implies \dot{\theta} &= \frac{p_\theta}{A}
\intertext{Substituting (\ref{eq:q4c3}) into (\ref{eq:q4c2}) gives:}
p_\phi &= A \dot{\phi} \sin^2 \theta + p_\psi \cos \theta\\
\dot{\phi} &= \frac{p_\phi - p_\psi \cos \theta}{A \sin^2 \theta}
\intertext{Rearranging (\ref{eq:q4c3}):}
\dot{\psi} &= \frac{p_\psi}{C} - \dot{\phi} \cos \theta\\
\dot{\psi} &= \frac{p_\psi}{C} - \lr{\frac{p_\phi - p_\psi \cos \theta}{A \sin^2 \theta}} \cos \theta
\end{align}

As usual, the Legendre transform \eqref{eq:Legendre} gives us $H$:
\begin{align}
H &= p_\theta \dot{\theta} + p_\phi \dot{\phi} + p_\psi \dot{\psi} - L\\
&= p_\theta \lr{\frac{p_\theta}{A}} + p_\phi \lr{\frac{p_\phi - p_\psi \cos \theta}{A \sin^2 \theta}} + p_\psi \lr{\frac{p_\psi}{C} - \lr{\frac{p_\phi - p_\psi \cos \theta}{A \sin^2 \theta}} \cos \theta}\\
&\qquad - \lr{\frac{1}{2} A \lr{\lr{\frac{p_\theta}{A}}^2 + \lr{\frac{p_\phi - p_\psi \cos \theta}{A \sin^2 \theta}}^2 \sin^2 \theta} + \frac{1}{2} C \lr{\frac{p_\psi}{C}}^2 - m g h \cos \theta}\\
&= \frac{p_\theta^2}{2A} + \lr{\frac{p_\phi - p_\psi \cos \theta}{A \sin^2 \theta}} \lr{p_\phi - p_\psi \cos \theta - \frac{1}{2} \lr{p_\phi - p_\psi \cos \theta}} + \frac{p_\psi^2}{2 C} + m g h \cos \theta
\end{align}
i.e.:
\begin{align}
H \lr{\theta, p_\theta, p_\phi, p_\psi} &= \frac{p_\theta^2}{2A} + \frac{\lr{p_\phi - p_\psi \cos \theta}^2}{2 A \sin^2 \theta} + \frac{p_\psi^2}{2 C} + m g h \cos \theta
\end{align}

For equations of motion:
\begin{align}
\dot{\theta} &= \pdv{H}{p_\theta} = \frac{p_\theta}{A}\\
\dot{\phi} &= \pdv{H}{p_\phi} = \frac{p_\phi - p_\psi \cos \theta}{A \sin^2 \theta}\\
\dot{\psi} &= \pdv{H}{p_\psi} = \frac{p_\psi}{C} - \frac{\lr{p_\phi - p_\psi \cos \theta} \cos \theta}{A \sin^2 \theta}\\
\dot{p}_\theta &= - \pdv{H}{\theta}\\
&= - \Bigg[2 \lr{\frac{p_\phi - p_\psi \cos \theta}{2 A \sin^2 \theta}} \lr{p_\psi \sin \theta}\\
\label{eq:q4c4} &\qquad - 2 \lr{\frac{\lr{p_\phi - p_\psi \cos \theta}^2}{2 A}} \lr{\frac{\cos \theta}{\sin^3 \theta}} - m g h \sin \theta \Bigg]\\
&= \lr{\frac{p_\phi - p_\psi \cos \theta}{A \sin^3 \theta}} \lr{\lr{p_\phi - p_\psi \cos \theta} \cos \theta - p_\psi \sin^2 \theta} + m g h \sin \theta
\intertext{i.e.:}
\label{eq:q4c5} \dot{p}_\theta &= \frac{\lr{p_\phi - p_\psi \cos \theta} \lr{p_\phi \cos \theta - p_\psi}}{A \sin^3 \theta} + m g h \sin \theta
\intertext{and:}
\label{eq:q4c6} \dot{p}_\phi &= - \pdv{H}{\phi} = 0\\
\label{eq:q4c7} \dot{p}_\psi &= - \pdv{H}{\psi} = 0
\end{align}
%\textcolor{red}{NB: (\ref{eq:q4c4}), (\ref{eq:q4c6}) and (\ref{eq:q4c7}) introduce different signs to handwritten answers, so (\ref{eq:q4c5}) negated.}

%5
\section{}

\begin{figure}[ht]
\begin{center}
\begin{tikzpicture}[scale=1.35]
% Drawing
\draw (-2,-1) rectangle(2,3);
\draw (0,1) circle(1);
% Axes
\draw[ultra thin, gray, ->, dashed] (-1,1) -- (3.5,1) node[black, right]{D};
% Labels
\node[above, gray] at (-0.5,1) {$a$};
\node[above, gray] at (0.5,1) {$a$};
\filldraw[fill=white!80!red] (-1,1) circle(0.05) node[left]{A};
\filldraw(1,1) circle(0.05) node[below right]{B};
\filldraw(0,1) circle(0.05) node[below]{C};
\end{tikzpicture}
\caption{The insect starts from B at $t=0$}
\label{fig:Q5_Fig1}
\end{center}
\end{figure}

\subsection{}

Since the paper is fixed at $A$, we treat $A$ as the origin of a fixed inertial frame. We then define the position vector, $\vec{P} \lr{\rho, \phi}$, as the polar co-ordinates of the insect with respect to this frame (see fig \ref{fig:Q5_Fig2}).

\begin{figure}[ht]
\begin{center}
\begin{tikzpicture}[scale=1.5]
% Drawing
\draw[rotate=-35] (-2,-1) rectangle(2,3);
\draw[rotate=-35] (0,1) circle(1);
\draw[rotate=-35, ultra thin, gray, dashed] (0,0) -- (0,2);
\draw[rotate=-35, ultra thin, gray, dashed] (0,0) -- ([shift=(160:1)]0,1) -- (0,1);
% Axes
\draw[ultra thin, gray, ->, dashed] (0,0) -- (3.5,0) node[black, right]{D};
% Labels
\node at (0.3,0.15) {$q$};
\filldraw[rotate=-35, fill=white!80!red] (0,0) circle(0.05) node[below left]{A};
\filldraw[rotate=-35] (0,2) circle(0.05) node[above right]{B};
\filldraw[rotate=-35] (0,1) circle(0.05) node[right]{C};
\filldraw[rotate=-35] ([shift=(160:1)]0,1) circle(0.05) node[left]{P};
\draw[rotate=-35, red,dashed,->,>=stealth,semithick] (-60:0.5) arc (-60:240:0.5);
\draw[rotate=-35, blue,dashed,->,>=stealth,semithick] ([shift=(90:1.1)]0,1) arc (90:160:1.1);
\end{tikzpicture}
\caption{Paper rotates about A as insect walks from B to P}
\label{fig:Q5_Fig2}
\end{center}
\end{figure}

\subsubsection{Determine $\phi$}

The insect's azimuthal co-ordinate, $\phi$, is:
\begin{align}
\phi &= \angle P A D\\
&= \angle P A C + \angle C A D\\
\label{eq:q5a1} &= \angle P A C + q
\intertext{because the question defines our generalised co-ordinate, $q= \angle C A D$.}
\intertext{The insect starts at $B$ at $t=0$, walking along the circle at uniform speed, $s$:}
s &= 2 a \Omega
\intertext{so the length of the arc from $B$ to $P$ at time, $t$, is:}
\label{eq:q5as} BP &= 2 a \Omega t
\intertext{Since the circle's radius is $a$, the angle the arc subtends is:}
\angle P C B &= 2 \Omega t
\intertext{The exterior angle is thus:}
\angle P C A &= \pi - 2 \Omega t
\intertext{since $\triangle PCA$ is isosceles:}
\label{eq:q5a2} \angle P A C = \angle A P C &= \frac{1}{2} \lr{ \pi - \lr{\pi - 2 \Omega t}} = \Omega t
\intertext{and from (\ref{eq:q5a1}):}
\label{eq:q5a3} \phi &= q + \Omega t
\intertext{and hence:}
\label{eq:q5a4} \dot{\phi} &= \dot{q} + \Omega
\end{align}

\subsubsection{Determine $\rho$}

The insect's radial co-ordinate, $\rho$, is:
\begin{align}
\rho &= \abs{\overline{\vb{AP}}}
\intertext{and since $\triangle PCA$ is isosceles:}
\cos \angle P A C &= \frac{\flatfrac{\rho}{2}}{a}
\intertext{rearranging for $\rho$ and using (\ref{eq:q5a2}):}
\label{eq:q5a5} \rho &= 2 a \cos \Omega t
\intertext{and hence:}
\label{eq:q5a6} \dot{\rho} &= - 2 a \Omega \sin \Omega t
\end{align}

\subsection{Determine the Lagrangian}

The insect is constrained to the table surface, so the potential energy, $V = 0$.

We construct the Lagrangian from the kinetic energy of the ant and the rotating paper:
\begin{align}
L &= \frac{1}{2} m \dot{\vec{P}}^2 + \frac{1}{2} \lr{4 m a^2} \dot{q}^2
\intertext{re-using the metric from question 3, (\ref{eq:q3m1}) - (\ref{eq:q3m2}):}
L &= \frac{1}{2} m \lr{\dot{\rho}^2 + \rho^2 \dot{\phi}^2} + 2 m a^2 \dot{q}^2
\intertext{using \eqref{eq:q5a4}, \eqref{eq:q5a5} and \eqref{eq:q5a6}:}
L &= \frac{1}{2} m \lr{\lr{- 2 a \Omega \sin \Omega t}^2 + \lr{2 a \cos \Omega t}^2 \lr{\dot{q} + \Omega}^2} + 2 m a^2 \dot{q}^2\\
&= 2 m a^2 \lr{\Omega^2 \sin^2 \Omega t + \cos^2 \Omega t \lr{\dot{q}^2 + 2 \dot{q} \Omega + \Omega^2} + \dot{q}^2}\\
&= 2 m a^2 \lr{\Omega^2 + \dot{q}^2 \cos^2 \Omega t + 2 \dot{q} \Omega \cos^2 \Omega t + \dot{q}^2}\\
&= 2 m a^2 \lr{\dot{q}^2 \lr{1 + \cos^2 \Omega t} + 2 \dot{q} \Omega \cos^2 \Omega t + \Omega^2}
\intertext{and we are free to drop the constant term from our Lagrangian giving the form requested:}
\label{eq:q5b1} L \lr{\dot{q}, t} &= 2 m a^2 \lrb{\lr{1 + \cos^2 \Omega t} \dot{q}^2 + 2 \Omega \dot{q} \cos^2 \Omega t}
\end{align}

\subsection{Determine the conjugate momentum}

The momentum, $p$, conjugate to $q$ is:
\begin{align}
\label{eq:q5c1} p &= \pdv{L}{\dot{q}}\\
&= 4 m a^2 \lrb{\lr{1 + \cos^2 \Omega t} \dot{q} + \Omega \cos^2 \Omega t}
\intertext{which we re-arrange:}
 \lr{1 + \cos^2 \Omega t} \dot{q} &= \frac{p}{4 m a^2} - \Omega \cos^2 \Omega t
 \intertext{i.e.:}
\label{eq:q5c2} \dot{q} &= \frac{p - 4 m a^2 \Omega \cos^2 \Omega t}{4 m a^2 \lr{1 + \cos^2 \Omega t}}
\end{align}

\subsection{Determine the Hamiltonian}

We determine the Hamiltonian in the usual way, using the Legendre transform, (\ref{eq:q5b1}), (\ref{eq:q5c1}) and (\ref{eq:q5c2}) [and a judicious choice to leave the overall factor of $\dot{q}$ unexpanded until (\ref{eq:q5d1})]:
\begin{align}
H &= p \dot{q} - L\\
&= p \dot{q} - 2 m a^2 \dot{q} \lrb{\lr{1 + \cos^2 \Omega t} \lr{\frac{p - 4 m a^2 \Omega \cos^2 \Omega t}{4 m a^2 \lr{1 + \cos^2 \Omega t}}} + 2 \Omega \cos^2 \Omega t}\\
&= \lr{p - 2 m a^2 \lrb{\frac{p}{4 m a^2} - \Omega \cos^2 \Omega t + 2 \Omega \cos^2 \Omega t}} \dot{q}\\
\label{eq:q5d1} &= \lr{\frac{1}{2} p - 2 m a^2 \Omega \cos^2 \Omega t} \lr{\frac{p - 4 m a^2 \Omega \cos^2 \Omega t}{4 m a^2 \lr{1 + \cos^2 \Omega t}}}\\
&= \frac{\lr{p - 4 m a^2 \Omega \cos^2 \Omega t}^2}{8 m a^2 \lr{1 + \cos^2 \Omega t}}
\intertext{and since we are free to define the zero point of energy wherever we like:}
H \lr{p, t} &= \frac{\lr{p - 4 m a^2 \Omega \cos^2 \Omega t}^2}{8 m a^2 \lr{1 + \cos^2 \Omega t}} + \textrm{const}
\end{align}

\subsection{Hamilton's equations of motion}

\begin{align}
\dot{p} &= - \pdv{H}{q} = 0 \implies p = \textrm{const}\\
\label{eq:q5e1} \dot{q} &= \pdv{H}{p} = \frac{p - 4 m a^2 \Omega \cos^2 \Omega t}{4 m a^2 \lr{1 + \cos^2 \Omega t}}
\intertext{which agrees with (\ref{eq:q5c2}).}
\intertext{We are given $q = \dot{q} = 0$ at $t=0$, so (\ref{eq:q5e1}) gives:}
p &= 4 m a^2 \Omega
\intertext{so we rearrange (\ref{eq:q5e1}):}
\dot{q} &= \Omega \lr{\frac{1 - \cos^2 \Omega t}{1 + \cos^2 \Omega t}}
\intertext{The distance on the arc from $A$ to $B$ is $\pi a$, so using (\ref{eq:q5as}), we get the time, $T_A$ to reach $A$:}
T_A &= \frac{\pi a}{2 a \Omega} = \frac{\pi}{2 \Omega}
\intertext{We integrate $\dot{q}$ to get the angle our paper turns through:}
q \lr{T_a} - q \lr{0} &= \int_0^{T_A} \dot{q} \dd{t}\\
&= \int_0^{\flatfrac{\pi}{2 \Omega}} \lr{\frac{1 - \cos^2 \Omega t}{1 + \cos^2 \Omega t}} \Omega \dd{t}
\intertext{changing variables:}
\theta &= \Omega t\\
\dd{\theta} &= \Omega \dd{t}\\
q \lr{T_a} - q \lr{0} &= \int_0^{\flatfrac{\pi}{2}} \lr{\frac{1 - \cos^2 \theta}{1 + \cos^2 \theta}} \dd{\theta}\\
&= \int_0^{\flatfrac{\pi}{2}} \lr{\frac{2}{1 + \cos^2 \theta} - 1} \dd{\theta}\\
&= \frac{2 \pi}{2 \sqrt{2}} - \frac{\pi}{2}
\intertext{using the integral provided.}
\intertext{i.e. by the time the insect reaches $A$, the paper will have turned through an angle of:}
q \lr{T_a} - q \lr{0} &= \frac{\pi}{2} \lr{\sqrt{2} - 1}
\end{align}

%\begin{align}
%\end{align}
%\begin{equation}
%\end{equation}

% Table of contents
%\tableofcontents

% Appendices follow
\appendix \section*{Appendices} \addcontentsline{toc}{section}{Appendices} \def\thesection{\arabic{section}}
\section{Generalised coordinates and the invariant measure} \label{apdx:dotprod}

For a more complete description, see \cite[\S3.10, pp 183-4]{ArfkenWeberHarris}, but to summarise, for a generalised co-ordinate system:
\begin{align}
\vec{q} &= \lr{q_1, q_2, \ldots, q_n}
\intertext{the Cartesian co-ordinates can be found as functions of the generalised co-ordinates:}
&\quad x \lr{q_1, q_2, \ldots, q_n}\\
&\quad y \lr{q_1, q_2, \ldots, q_n}\\
&\quad z \lr{q_1, q_2, \ldots, q_n}
\intertext{and for the metric spaces we are interested in, we compute the metric, $g_{ij}$:}
\label{eq:apdx_metric} g_{ij} \lr{q_1, q_2, \ldots, q_n} &= \pdv{x}{q_i} \pdv{x}{q_j} + \pdv{y}{q_i} \pdv{y}{q_j} + \pdv{z}{q_i} \pdv{z}{q_j}
\intertext{so that for vectors of generalised co-ordinates:}
\vec{a} &= \lr{a_1, a_2, \ldots, a_n}\\
\vec{b} &= \lr{b_1, b_2, \ldots, b_n}
\intertext{the dot product, defined as:}
\vec{a} \cdot \vec{b} &= \sum_{i,j} g_{ij} a_i b_j
\end{align}
is the invariant measure (i.e. the dot product produces the same results as if we converted to Cartesian co-ordinates first).

With a restriction to orthogonal co-ordinate systems, the differential operators (gradient, divergence, curl, laplacian) can also be suitably defined to ensure they are invariant, see \cite[\S3.10, pp 185 et seq.]{ArfkenWeberHarris}.

% Cite my reference library (test)
%\cite{AitchisonHey} \cite{AitchisonHey:1} \cite{AitchisonHey:2} \cite{ArfkenWeberHarris} \cite{Bauer} \cite{Becker} \cite{BjorkenDrell} \cite{BurgessMoore} \cite {ByronFuller} \cite{Cardy} \cite{ChaikinLubensky} \cite{ChengLi} \cite{ChengLiProbs} \cite{CottinghamGreenwood} \cite{Efron} \cite{EllisStirlingWebbers} \cite{FeynmanHibbs} \cite{Foot} \cite{GattringerLang} \cite{Georgi} \cite{GoldsteinSafkoPoole} \cite{GriffithsE} \cite{GriffithsQ} \cite{Grindrod} \cite{HobsonEfstathiouLasenby} \cite{James} \cite{Jeevanjee} \cite{Jones} \cite{Kadanoff} \cite{KibbleBerkshire} \cite{Kleinert} \cite{Leader} \cite{Leader:1} \cite{Leader:2} \cite{LesHouches:93} \cite{Lyons} \cite{Maggiore} \cite{MandlShaw} \cite{Okun} \cite{Paschos} \cite{PercivalRichards} \cite{PeskinSchroeder} \cite{Purdy} \cite{RileyHobson} \cite{RileyHobson:s} \cite{Roberts} \cite{Roberts:A} \cite{Rothe} \cite{SakuraiNapolitano} \cite{Schwartz} \cite{Schweber} \cite{Shankar} \cite{Srednicki} \cite{Stroustrup} \cite{WeinbergGrav} \cite{Weinberg} \cite{Weinberg:1} \cite{Weinberg:2} \cite{Weinberg:3} \cite{WeinbergGrav} \cite{Zee:1} \cite{Zee:2} \cite{ZinnJustin}\\
% Cite sample journal article I took the citation from DiscoverEd
%\cite{HammondRichardT2015Nm}
% Cite samples from latex template
%\cite{seger}

% Print the bibliography
%\addcontentsline{toc}{section}{References}
\printbibliography

\end{document}
