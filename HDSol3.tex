\documentclass[12pt,twoside,a4paper]{article}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
%\usepackage[makeroom]{cancel}
\usepackage{cancel}
\usepackage{color}
\usepackage{fancyhdr}
\usepackage[a4paper,left=2cm,right=2cm]{geometry}
\usepackage{graphicx}
\usepackage[colorlinks]{hyperref}
\usepackage{ifthen}
\usepackage[utf8]{inputenc}
\usepackage{lastpage}
\usepackage{physics}
\usepackage{relsize}
\usepackage{simplewick}
\usepackage{subcaption}
\usepackage{tikz}
\usetikzlibrary{patterns}

%Biblatex
\usepackage[style=numeric,sorting=nyt,isbn=true,backend=biber]{biblatex}
\addbibresource{HDSol.bib}

% Define some commands I use regularly
\newcommand*{\lr}[1]{\left( {#1} \right)}
\newcommand*{\lrb}[1]{\left[ {#1} \right]}
\newcommand*{\lrc}[1]{\left\{ {#1} \right\}}
\newcommand*{\e}{\operatorname{e}}
\newcommand*{\im}{\operatorname{i}}
\newcommand*{\qop}[1]{\mathrm{\widehat{#1}}}
\newcommand*{\diracdelta}[2][]{\, \delta \ifthenelse{\equal{#1}{}}{}{^{\left({#1}\right)}} \left( {#2} \right)}
\newcommand*{\dx}[1][]{\dd[#1]{x}}
\newcommand*{\pathint}[1][]{\int \mathcal{D}{#1}\,}
\DeclareMathOperator{\cosec}{cosec}
% Blackboard 1 = \mathbbold{1}
\newcommand{\bbfamily}{\fontencoding{U}\fontfamily{bbold}\selectfont}
\DeclareMathAlphabet{\mathbbold}{U}{bbold}{m}{n}

% style allowing styles to be applied to each segment of a path
% http://tex.stackexchange.com/questions/3161/tikz-how-to-draw-an-arrow-in-the-middle-of-the-line
\usetikzlibrary{decorations.pathreplacing,decorations.markings}
\tikzset{
  on each segment/.style={
    decorate,
    decoration={
      show path construction,
      moveto code={},
      lineto code={
        \path [#1]
        (\tikzinputsegmentfirst) -- (\tikzinputsegmentlast);
      },
      curveto code={
        \path [#1] (\tikzinputsegmentfirst)
        .. controls
        (\tikzinputsegmentsupporta) and (\tikzinputsegmentsupportb)
        ..
        (\tikzinputsegmentlast);
      },
      closepath code={
        \path [#1]
        (\tikzinputsegmentfirst) -- (\tikzinputsegmentlast);
      },
    },
  },
  % style to add an arrow in the middle of a path
  mid arrow/.style={postaction={decorate,decoration={
        markings,
        mark=at position .5 with {\arrow[#1]{stealth}}
      }}},
}

%Now define values for my document layout and header
\pagestyle{fancy}
\setlength{\voffset}{-0.5in}
\addtolength{\textheight}{89pt}
\setlength{\headheight}{33pt}
\parindent 0pt
\parskip 12pt
\title{PHYS11012 Hamiltonian Dynamics}
\lfoot{PHYS11012 Hamiltonian Dynamics}
\author{Example Sheet 3 Solutions}
\rfoot{Example Sheet 3 Solutions}
\date{Fri 5$^{\mathrm{th}}$ Feb 2021}
\cfoot{\thepage{} of \pageref{LastPage}}
\lhead{\includegraphics[scale=0.4]{crest.pdf}}

\begin{document}
\thispagestyle{empty}
\maketitle
\begin{center}
\includegraphics{crest.pdf}\\
\end{center}

% Section headings: Question n
\def\thesection{Q\arabic{section}}
%\def\thesection{\arabic{section}}
\def\thesubsection{\arabic{section}.\alph{subsection}}
\def\thesubsubsection{\arabic{section}.\alph{subsection}.\roman{subsubsection}}

Latest version: \href{https://git.ecdf.ed.ac.uk/s1786208/HD21}{https://git.ecdf.ed.ac.uk/s1786208/HD21}

% Table of contents
\tableofcontents

\newpage
%1
\section{Pendulum of grandfather clock}

Taking $\theta$ as our generalised coordinate for a light rod of length $l$ with mass $m$ at the end:
\begin{figure}[ht]
\begin{center}
\begin{tikzpicture}
% Axes
\draw[ultra thin, gray, ->, dashed] (0,0) -- (0,-5);
% Rod
\draw[ultra thick, blue] (0,0) -- (1.5,-2) node[right]{$l$} -- (3, -4);
\filldraw[blue] (3,-4) circle(0.07);
%mg
\draw[ultra thin, gray, ->, dashed] (3,-4) -- (3,-5) node[below]{$m g$};
% Angles
\draw[gray,->,>=stealth,semithick] ([shift=(-90:1.2)]0,0) arc (-90:-53:1.2);
\node[black] at (0.25,-0.72) {$\theta$};
\end{tikzpicture}
\caption{Pendulum of grandfather clock}
\label{fig:q1fig1}
\end{center}
\end{figure}

Our Kinetic energy, $T$, and potential, $V$, are:
\begin{align}
T &= \frac{1}{2} m l^2 \dot{\theta}^2\\
V &= - m g l \cos \theta
\end{align}
I plot the potential below (``1.0''$=m g l$):
\begin{figure}[ht]
\begin{center}
\includegraphics{{HDSol3Fig2}.pdf}\\
\caption{$V = - m g L \cos \lr{\theta}$, $\theta \in \lrb{-\pi, \pi}$}
\label{fig:q1fig2}
\end{center}
\end{figure}

As the question notes, we have normalised in such a way that our energy can be negative, i.e.:
\begin{align}
V_0 \equiv V \lr{0} &= - m g l
\intertext{\textcolor{red}{We can ignore} the more usual normalisation $V_0 = 0$, for which we would have had to define:}
V &= mgl \lr{1 - \cos \theta}
\intertext{Our lagrangian, $L$, is:}
L &= T - V\\
&= \frac{1}{2} m l^2 \dot{\theta}^2 + m g l \cos \theta
\intertext{so our momentum conjugate to $\theta$, $p_\theta$, is:}
p_\theta &\equiv \pdv{L}{\dot{\theta}}\\
&= m l^2 \dot{\theta}
\intertext{which we rearrange for $\dot{\theta}$:}
\dot{\theta} &= \frac{p_\theta}{m l^2}
\intertext{Our Hamiltonian is therefore:}
H \lr{\theta, p_\theta} &= T + V\\
&= \frac{1}{2} m l^2 \lr{\frac{p_\theta}{m l^2}}^2 - m g l \cos \theta\\
\intertext{i.e. as we were asked to find:}
\label{eq:q1a1} H &= \frac{p_\theta^2}{2 m l^2} - m g l \cos \theta \equiv E
\end{align}
which is independent of time, hence is our constant energy $E$, defining orbits in phase space.\\
\hrule

Rearranging \eqref{eq:q1a1} for $p_\theta$
\begin{align}
\label{eq:q1a2} p_\theta &= \pm \sqrt{2 m l^2 \lr{\color{red} E + m g l \cos \theta}}
\end{align}

\subsection{$\abs{E} < m g l$ -- libration}

This case arises from noticing that \eqref{eq:q1a2} might have complex solutions unless:
\begin{align}
0 &\le 2 m l^2 \lr{E + m g l \cos \theta}
\intertext{and since $m > 0$:}
0 &\le E + m g l \cos \theta\\
\cos \theta &\ge - \frac{E}{m g l}
\intertext{and since $\cos \theta$ takes it's maximum value when $\abs{\theta}$ takes it's minimum, this means there is some \emph{maximum} angle, $\theta_{\textrm{max}}$, with:}
\label{eq:q1b1} \theta_{\textrm{max}} &= \cos^{-1} \lr{- \frac{E}{m g l}}
\intertext{and for which:}
\abs{\theta} &\le \abs{\theta_{\textrm{max}}}\\
\intertext{Physically, the pendulum librates (oscillates) between:}
- \theta_{\textrm{max}} &\le \theta \le \theta_{\textrm{max}}
\intertext{For \eqref{eq:q1b1} to have a solution, we must have:}
\abs{\frac{E}{m g l}} & \le 1
\intertext{i.e.:}
\abs{E} & \le m g l
\end{align}
So, for $\abs{E} < m g l$, there is a maximum angle, $\theta_{\textrm{max}}$ at which $p_{\theta_{\textrm{max}}}=0$, otherwise the expression in red in \eqref{eq:q1a1} would become negative and $p_\theta$ imaginary.

\subsection{$\abs{E} > m g l$ -- rotation}

In this case we have real solutions for \eqref{eq:q1a1} $\forall \theta$. If all values of $\theta$ are allowed, then the pendulum rotates.

See \url{https://demonstrations.wolfram.com/PhaseSpaceOfASimplePendulum/} \cite{Higgins:Pendulum}, the course texts, e.g. \cite[fig~4.4, pg 55]{PercivalRichards} or the handwritten notes for phase diagrams.

\section{Relativistic particle}

\subsection{Hamiltonian and energy conservation}

We are given the Lagrangian ($L = T - V$) for a free particle ($V = 0$), i.e. we are given the kinetic energy, $T$, of a relativistic particle:
\begin{align}
T &= - m c^2 \sqrt{1 - \lr{\frac{\dot{q}}{c}}^2}
\intertext{hence for the potential given, $V \lr{q} = \frac{1}{2} m \omega^2 q^2$, the Lagrangian is:}
L &= - m c^2 \sqrt{1 - \lr{\frac{\dot{q}}{c}}^2} - \frac{1}{2} m \omega^2 q^2
\intertext{By definition, the momentum, $p$, is:}
p &\equiv \pdv{L}{\dot{q}}\\
&= - \frac{m c^2}{2} \lr{1 - \lr{\frac{\dot{q}}{c}}^2}^{- \flatfrac{1}{2}} \lr{- 2 \frac{\dot{q}}{c^2}}\\
\intertext{i.e.:}
\label{eq:q2a1} p &= \frac{m \dot{q}}{\sqrt{1 - \lr{\frac{\dot{q}}{c}}^2}}
\intertext{and we note from \eqref{eq:q2a1} (since $\sqrt{\textrm{x}} \ge 0$, $x \in \mathbb{R}$) that the sign of $p$ is the same as $\dot{q}$.}
\intertext{Rearranging for $\dot{q}$:}
1 - \lr{\frac{\dot{q}}{c}}^2 &= \frac{m^2 \dot{q}^2}{p^2}\\
\dot{q}^2 \lr{\frac{m^2}{p^2} + \frac{1}{c^2}} &= 1\\
\label{eq:q2a2_sq} \dot{q}^2 &= \frac{p^2 c^2}{m^2 c^2 + p^2}
\intertext{and we take the positive root to maintain the sign of $\dot{q}$ the same as $p$, i.e.:}
\label{eq:q2a2} \dot{q} &= \frac{p c}{\sqrt{m^2 c^2 + p^2}}
\end{align}

Taking a Legendre transform to get the Hamiltonian and using \eqref{eq:q2a2}:
\begin{align}
H &= p \dot{q} - L\\
&= \frac{p^2 c}{\sqrt{m^2 c^2 + p^2}} + m c^2 \sqrt{1 - \frac{p^2}{m^2 c^2 + p^2}} + \frac{1}{2} m \omega^2 q^2\\
&= \frac{p^2 c}{\sqrt{m^2 c^2 + p^2}} + m c^2 \sqrt{\frac{m^2 c^2}{m^2 c^2 + p^2}} + \frac{1}{2} m \omega^2 q^2\\
&= \frac{p^2 c + m^2 c^3}{\sqrt{m^2 c^2 + p^2}} + \frac{1}{2} m \omega^2 q^2\\
&= c \sqrt{m^2 c^2 + p^2} + \frac{1}{2} m \omega^2 q^2
\intertext{finally arriving at the Hamiltonian:}
H &= \sqrt{p^2 c^2 + m^2 c^4} + \frac{1}{2} m \omega^2 q^2
\intertext{The Hamiltonian is independent of time, or more formally (see notes, \S~2.2 pg~19):}
0 &= \dv{H}{t} - \pdv{H}{t}
\intertext{so yes, the energy, $E$, is conserved:}
\label{eq:q2a3} E &= \sqrt{p^2 c^2 + m^2 c^4} + \frac{1}{2} m \omega^2 q^2
\end{align}

\subsection{Period of oscillation}

Orbits have maximum displacement $q = q_{\textrm{max}}$ in phase-space when $p=0$, so \eqref{eq:q2a3} gives:
\begin{align}
E &= m c^2 + \frac{1}{2} m \omega^2 q_{\textrm{max}}^2
\intertext{i.e.:}
\label{eq:q2b1} q_{\textrm{max}} &= \sqrt{\frac{2}{m \omega^2} \lr{E - m c^2}}
\intertext{By symmetry, the period, $T$, is 4 times one quarter period (see section 2.5.1 of the notes on periodic motion, pg 21):}
\label{eq:q2b2} T &= 4 \int_0^{q_{\textrm{max}}} \frac{\dd{q}}{\dot{q}}
\intertext{We'll use \eqref{eq:q2a2} for $\dot{q}$, but first we'll need to rearrange \eqref{eq:q2a3} to eliminate $p$:}
p^2 c^2 &= \lr{E - \frac{1}{2} m \omega^2 q^2}^2 - m^2 c^4
\intertext{\eqref{eq:q2a2_sq} now gives:}
\dot{q}^2 &= \frac{\lr{E - \frac{1}{2} m \omega^2 q^2}^2 - m^2 c^4}{\frac{1}{c^2}\lr{m^2 c^4 + \lr{E - \frac{1}{2} m \omega^2 q^2}^2 - m^2 c^4}}\\
&= \frac{c^2 \lr{\lr{E - \frac{1}{2} m \omega^2 q^2}^2 - m^2 c^4}}{\lr{E - \frac{1}{2} m \omega^2 q^2}^2}\\
\intertext{Substituting this into \eqref{eq:q2b2} gives:}
\label{eq:q2b3} T &= \frac{4}{c} \int_0^{q_{\textrm{max}}} \dd{q} \frac{E - \frac{1}{2} m \omega^2 q^2}{\sqrt{\lr{E - \frac{1}{2} m \omega^2 q^2}^2 - m^2 c^4}}
\intertext{Now we introduce an angle $\theta$ such that:}
q &= q_{\textrm{max}} \sin \theta
\intertext{and we note (because we'll need it for the limit in \eqref{eq:q2b3}) that when $q = q_{\textrm{max}}$, $\theta = \flatfrac{\pi}{2}$.}
\intertext{so from \eqref{eq:q2b1}:}
\label{eq:q2b4} q &= \lr{\sqrt{\frac{2}{m \omega^2} \lr{E - m c^2}}} \sin \theta
\intertext{Looking at \eqref{eq:q2b3}, we start by evaluating the numerator:}
E - \frac{1}{2} m \omega^2 q^2 &= E - \frac{1}{2} m \omega^2 \lr{\frac{2}{m \omega^2} \lr{E - m c^2}} \sin^2 \theta\\
\label{eq:q2b5} &= E - \lr{E - m c^2} \sin^2 \theta
\intertext{Defining everything inside the square root of the denominator of \eqref{eq:q2b3} as $D$:}
D &= \lrb{E - \lr{E - m c^2} \sin^2 \theta}^2 - m^2 c^4\\
&= \lrb{E - \lr{E - m c^2} \sin^2 \theta - m c^2} \lrb{E - \lr{E - m c^2} \sin^2 \theta + m c^2}\\
&= \lrb{\lr{E - m c^2} \lr{1 - \sin^2 \theta}} \lrb{E + m c^2 - \lr{E - m c^2} \sin^2 \theta}\\
\label{eq:q2b6} &= \lrb{\lr{E - m c^2} \cos^2 \theta} \lrb{E + m c^2 - \lr{E - m c^2} \sin^2 \theta}
\intertext{Now we need the derivative of \eqref{eq:q2b4}:}
\label{eq:q2b7} \dd{q} &= \lr{\sqrt{\frac{2}{m \omega^2} \lr{E - m c^2}}} \cos \theta \dd{\theta}
\intertext{Inserting \eqref{eq:q2b5}, \eqref{eq:q2b6} and \eqref{eq:q2b7} into \eqref{eq:q2b3}:}
T &= \frac{4}{c} \int_0^{\flatfrac{\pi}{2}} \dd{\theta} \frac{\lr{\sqrt{\frac{2}{m \omega^2} \lr{E - m c^2}}} \cos \theta \lr{E - \lr{E - m c^2} \sin^2 \theta}}{\sqrt{E - m c^2} \cos \theta \sqrt{E + m c^2 - \lr{E - m c^2} \sin^2 \theta}}
\intertext{and we finally arrive at the expression we were asked to find, i.e.:}
\label{eq:q2b8} T &= \frac{4}{\omega c} \sqrt{\frac{2}{m}} \int_0^{\flatfrac{\pi}{2}} \dd{\theta} \frac{E - \lr{E - m c^2} \sin^2 \theta}{\sqrt{E + m c^2 - \lr{E - m c^2} \sin^2 \theta}}
\end{align}
\hrule

When:
\begin{align}
\abs{\dot{q}} &\ll c
\intertext{then we can ignore $\flatfrac{\dot{q}}{c}$ in \eqref{eq:q2a1} giving:}
p &\simeq m \dot{q}
\intertext{i.e.:}
p^2 &\ll m^2 c^2
\intertext{Bearing this in mind while taking $q=0$ in \eqref {eq:q2a3} gives:}
E &\simeq m c^2
\intertext{Substituting $E = m c^2$ into \eqref{eq:q2b8} gives:}
T &= \frac{4}{\omega c} \sqrt{\frac{2}{m}} \int_0^{\flatfrac{\pi}{2}} \dd{\theta} \frac{m c^2}{\sqrt{2 m c^2}}\\
T &= \frac{4}{\omega c} \sqrt{\frac{2}{m}} \lr{\frac{\pi}{2}} c\sqrt{\frac{m}{2}}\\
\intertext{Yielding the expected result:}
T &= \frac{2 \pi}{\omega}
\end{align}

\section{Ensemble of particles in a rectangle in phase-space}

For one particle, the equations of motion are:
\begin{align}
\ddot{q} &= g\\
p &= m \dot{q}
\intertext{and given the Dirichlet boundary conditions stated, have the following solutions:}
\label{eq:q3a1} q \lr{t} &= q_0 + \frac{p_0}{m} t + \frac{1}{2} g t^2\\
\label{eq:q3a2} p \lr{t} &= p_0 + m g t
\intertext{To eliminate $t$, we rearrange the expression for $p \lr{t}$ for $t$:}
t &= \frac{p - p_0}{m g}
\intertext{Then substitute this into the expression for $q \lr{t}$:}
q &= q_0 + \frac{p_0}{m} \lr{\frac{p - p_0}{m g}} + \frac{g}{2} \lr{\frac{p - p_0}{m g}}^2\\
&= q_0 - \frac{p_0^2}{m^2 g} + \cancel{\color{red} \frac{p p_0}{m^2 g}} + \frac{1}{2 m^2 g} \lr{p^2 - \cancel{\color{red} 2 p p_0} + p_0^2}\\
&= q_0 - \frac{p_0^2}{2m^2 g} + \frac{p^2}{2 m^2 g}
\intertext{Which we rearrange to tell us how our particles move in phase space:}
p^2 &= 2 m^2 g \lr{q - q_0} + p_0^2
\intertext{These are parabolas in phase-space, and the initial area, $A_0$, is:}
\label{eq:q3a3} A_0 &= 4 a b
\end{align}
\begin{figure}[ht]
\centering
\begin{tikzpicture}[xscale=1,yscale=1]
%axes
\draw[ultra thin, gray, ->] (0, -4) -- (0, 4) node[left, black]{$p$};
\draw[ultra thin, gray, ->] (-5, 0) -- (5, 0) node[above, black]{$q$};
% labels
\node at (-4,0) [below left] {$-a$};
\node at (4,0) [below right] {$a$};
\node at (0.1, -2) [below left] {$-b$};
\node at (0.1, 2) [above left] {$b$};
% box
\draw [ultra thick, green!80!blue] (-4,-2) -- (4,-2) node[below, right]{$A$} -- (4, 2) node[above, right]{$B$} -- (-4, 2) node[above, left]{$C$}  -- (-4, -2)node[below, left]{$D$} ;
%\draw[black, line width = 0.5mm, green!80!blue]   plot[smooth, domain=-sqrt(8):sqrt(8)] (0.5 * \x * \x, \x);
\path [rotate=90, draw=blue,thick,dashed,postaction={on each segment={mid arrow=red}}] (-4,-1) parabola[bend pos=0.5] bend +(0,4) +(8,0);
\path [rotate=90, draw=blue,thick,dashed,postaction={on each segment={mid arrow=red}}] (-4,-2) parabola[bend pos=0.5] bend +(0,4) +(8,0);
\path [rotate=90, draw=blue,thick,dashed,postaction={on each segment={mid arrow=red}}] (-4,-3) parabola[bend pos=0.5] bend +(0,4) +(8,0);
\path [rotate=90, draw=blue,thick,dashed,postaction={on each segment={mid arrow=red}}] (-4,-4) parabola[bend pos=0.5] bend +(0,4) +(8,0);
\path [rotate=90, draw=blue,thick,dashed,postaction={on each segment={mid arrow=red}}] (-4,-5) parabola[bend pos=0.5] bend +(0,4) +(8,0);
\path [rotate=90, draw=blue,thick,dashed,postaction={on each segment={mid arrow=red}}] (-4,-6) parabola[bend pos=0.5] bend +(0,4) +(8,0);
\end{tikzpicture}
\caption{Particles within $ABCD$ at $t=0$ move along the parabolas in phase-space}
\label{fig:q3fig1}
\end{figure}

For a given $t$, \eqref{eq:q3a2} tells us that $p$ is a function of $p_0$, while \eqref{eq:q3a1} tells us that $q$ is a function of both $p_0$ and $q_0$. So we consider $p_0 = \pm b$ boundaries \ldots since they map to boundaries of constant $p$.

\begin{figure}[ht]
\centering
\begin{tikzpicture}[xscale=1,yscale=1]
%axes
\draw[ultra thin, gray, ->] (0, -1) -- (0, 6) node[left, black]{$p$};
\draw[ultra thin, gray, ->] (-1, 0) -- (12, 0) node[above, black]{$q$};
% box
\draw [ultra thick, green!80!blue] (1,1) -- (9,1) node[below, right]{$A$} -- (11, 5) node[above, right]{$B$} -- (3, 5) node[above, left]{$C$}  -- (1, 1)node[below, left]{$D$} ;
% coordinates of corners
\node at (9, 0.5) {$\lr{a - \frac{b}{m} t + \frac{1}{2} g t^2, -b + m g t}$};
\node at (11, 5.5) {$\lr{a + \frac{b}{m} t + \frac{1}{2} g t^2, b + m g t}$};
\node at (3, 5.5) {$\lr{- a + \frac{b}{m} t + \frac{1}{2} g t^2, b + m g t}$};
\node at (0.77, 0.5) {$\lr{-a - \frac{b}{m} t + \frac{1}{2} g t^2, -b + m g t}$};
\end{tikzpicture}
\caption{At a later time $t$, the particles are now within the parallelogram $ABCD$}
\label{fig:q3fig2}
\end{figure}
\newpage
On the boundary $CB$, initial conditions $p_0 = b$, $q_0 \in \lrb{-a, a}$ yield (from \eqref{eq:q3a1} and \eqref{eq:q3a2}):
\begin{align}
q \lr{t} &= q_0 + \frac{b}{m} t + \frac{1}{2} g t^2\\
p \lr{t} &= b + m g t
\intertext{whereas on the boundary $DA$, initial conditions $p_0 = -b$, $q_0 \in \lrb{-a, a}$ yield:}
q \lr{t} &= q_0 - \frac{b}{m} t + \frac{1}{2} g t^2\\
p \lr{t} &= -b + m g t
\intertext{The area of the parallelogram at time $t$, $A \lr{t}$, is its base $\times$ height, i.e.:}
A &= \lrb{a - \frac{b}{m} t + \frac{1}{2} g t^2 - \lr{-a - \frac{b}{m} t + \frac{1}{2} g t^2}} - \lrb{b + m g t - \lr{- b + m g t}}\\
&= \lr{2 a} \lr{2 b}\\
&= 4ab
\end{align}
which is the same as the initial Area, $A_0$ per \eqref{eq:q3a3}.

I.e. as expected (per Liouville's theorem version II, notes pg 24) the area in phase-space is unchanged.

\section{Covariance of Hamilton's equations}

As a reminder on cancelling the dots:
\begin{align}
\dot{q}_i' &\equiv \dv{q_i'}{t}\\
&= \pdv{q_i'}{q_j} \dv{q_j}{t} + \pdv{q_i'}{t} \dv{t}{t}\\
&= \pdv{q_i'}{q_j} \dv{q_j}{t} + \pdv{q_i'}{t}\\
\intertext{hence:}
\label{eq:q4a1} \dot{q}_i' &= \pdv{q_i'}{q_j} \dot{q}_j + \pdv{q_i'}{t}
\intertext{Hence (because the second term is independent of $\dot{q}_j$):}
\dv{\dot{q}_i'}{\dot{q}_j} &= \pdv{q_i'}{q_j}
\intertext{Given a point transformation under which the Lagrangian is invariant (dynamics unchanged):}
q_i' &= q_i' \lr{\vec{q}, t}\\
\label{eq:q4aL} L' \lr{\vec{q}\,', \dot{\vec{q}}\,', t} &= L \lr{\vec{q}, \dot{\vec{q}}, t}
\intertext{We start by examining the conjugate momenta:}
p_i &\equiv \pdv{L}{\dot{q}_i}\\
&= \pdv{L'}{\dot{q}_i}\\
&= \pdv{L'}{\dot{q}_j'} \pdv{\dot{q}_j'}{\dot{q}_i}\\
\intertext{i.e. cancelling the dots:}
\label{eq:q4a2} p_i &= p_j' \pdv{q_j'}{q_i}
\end{align}\\
\hrule

We find the transformed Hamiltonian with a Legendre transform:
\begin{align}
H' &= p_i' \dot{q}_i' - L'
\intertext{from \eqref{eq:q4a1}:}
H' &= p_i' \lr{\pdv{q_i'}{q_j} \dot{q}_j + \pdv{q_i'}{t}} - L
\intertext{using \eqref{eq:q4a2}:}
H' &= p_j \dot{q}_j - L + p_i' \pdv{q_i'}{t}
\intertext{i.e.:}
\label{eq:q4b1} H' &= H + p_i' \pdv{q_i'}{t}
\end{align}\\
\hrule

We evaluate the first of the expressions provided by starting with \eqref{eq:q4b1}:
\begin{align}
\pdv{H'}{p_i'} - \dot{q}_i' &= \pdv{p_i'} \lr{H + p_j' \pdv{q_j'}{t}} - \dot{q}_i'\\
&= \pdv{H}{p_i'} + \pdv{q_i'}{t} - \dot{q}_i'
\intertext{Using the chain rule and \eqref{eq:q4a1}:}
\label{eq:q4c1} \pdv{H'}{p_i'} - \dot{q}_i' &= \pdv{H}{q_j} {\color{red}\pdv{q_j}{p_i'}} + \pdv{H}{p_j} \pdv{p_j}{p_i'} + \bcancel{\color{red}\pdv{q_i'}{t}} - \lr{\pdv{q_i'}{q_j} \dot{q}_j + \bcancel{\color{red}\pdv{q_i'}{t}}}
\intertext{Now $\pdv*{q_j}{p_i'} = 0$ in the first term because in point transformations $q_i = q_i \lr{\vec{q}\,', t}$.}
\intertext{Differentiating \eqref{eq:q4a2} with respect to $p_j'$:}
\pdv{p_i}{p_j'} &= \pdv{q_j'}{q_i}
\intertext{So \eqref{eq:q4c1} becomes:}
\pdv{H'}{p_i'} - \dot{q}_i' &= \pdv{H}{p_j} \pdv{q_i'}{q_j} - \pdv{q_i'}{q_j} \dot{q}_j
\intertext{i.e. as we were asked to show:}
\label{eq:q4c2} \pdv{H'}{p_i'} - \dot{q}_i' &= \lr{\pdv{H}{p_j} - \dot{q}_j} \pdv{q_i'}{q_j}
\end{align}\\
\hrule

Similarly, we use \eqref{eq:q4b1} and take the full time derivative of \eqref{eq:q4a2}:
\begin{align}
\pdv{H}{q_i} + \dot{p}_i &= \pdv{q_i} \lr{H' - p_j' \pdv{q_j'}{t}} + \dot{p}_j' \pdv{q_j'}{q_i} + p_j' \dv{t} \lr{\pdv{q_j'}{q_i}}
\intertext{Using the chain rule:}
\pdv{H}{q_i} + \dot{p}_i &= \pdv{H'}{q_j'} \pdv{q_j'}{q_i} + \pdv{H'}{p_j'} \pdv{p_j'}{q_i} - \pdv{p_j'}{q_i} \pdv{q_j'}{t} - \bcancel{\color{red} p_j' \pdv{q_j'}{q_i}{t}}\\
&\qquad + \dot{p}_j' \pdv{q_j'}{q_i} + p_j' \pdv{q_j'}{q_i}{q_k} \dot{q}_k + \bcancel{\color{red} p_j' \pdv{q_j'}{q_i}{t}}\\
&= \lr{\pdv{H'}{q_j'} + \dot{p}_j'} \pdv{q_j'}{q_i} + \lr{\pdv{H'}{p_j'} - \pdv{q_j'}{t}} {\color{red} \pdv{p_j'}{q_i}}\\
&\qquad + \dot{q}_k \lrb{\pdv{q_i} \lr{p_j' \pdv{q_j'}{q_k}} - {\color{red} \pdv{p_j'}{q_i}} \pdv{q_j'}{q_k}}\\
&= \lr{\pdv{H'}{q_j'} + \dot{p}_j'} \pdv{q_j'}{q_i} + \lr{\pdv{H'}{p_j'} - \pdv{q_j'}{t} - \dot{q}_k \pdv{q_j'}{q_k}} \pdv{p_j'}{q_i} + \dot{q}_k \pdv{q_i} \lr{p_k}
\intertext{the last term vanishes because the $p_k$ and $q_i$ are independent, and we use \eqref{eq:q4a1} one last time to get the form we were asked for:}
\label{eq:q4d1} \pdv{H}{q_i} + \dot{p}_i &= \pdv{q_j'}{q_i} \lr{\pdv{H'}{q_j'} + \dot{p}_j'} + \pdv{p_j'}{q_i} \lr{\pdv{H'}{p_j'} - \dot{q}_j}
\end{align}\\
\hrule

If:
\begin{align}
\dot{q}_j &= \pdv{H}{p_j}
\intertext{then \eqref{eq:q4c2} gives:}
\dot{q}_i' &= \pdv{H'}{p_i'}
\intertext{I.e. the first of Hamilton's equations are covariant. \eqref{eq:q4d1} then yields:}
\label{eq:q4e1} \pdv{H}{q_i} + \dot{p}_i &= \pdv{q_j'}{q_i} \lr{\pdv{H'}{q_j'} + \dot{p}_j'}
\intertext{so that if:}
\dot{p}_j' &= -\pdv{H'}{q_j'}
\intertext{then \eqref{eq:q4e1} tells us that the second of Hamilton's equations is also covariant, i.e.:}
\dot{p}_i &= - \pdv{H}{q_i}
\intertext{i.e. if the point transformation is invertible, then Hamilton's equations are covariant.}
\intertext{NB: although \eqref{eq:q4aL}:}
L' &= L
\intertext{we have \eqref{eq:q4b1}:}
H' &\ne H
\end{align}

% Appendices follow
%\appendix \section*{Appendices} \addcontentsline{toc}{section}{Appendices} \def\thesection{\arabic{section}}
%\section{Generalised coordinates and the invariant measure} \label{apdx:dotprod}

% Cite my reference library (test)
%\cite{AitchisonHey} \cite{AitchisonHey:1} \cite{AitchisonHey:2} \cite{ArfkenWeberHarris} \cite{Bauer} \cite{Becker} \cite{BjorkenDrell} \cite{BurgessMoore} \cite {ByronFuller} \cite{Cardy} \cite{ChaikinLubensky} \cite{ChengLi} \cite{ChengLiProbs} \cite{CottinghamGreenwood} \cite{Efron} \cite{EllisStirlingWebbers} \cite{FeynmanHibbs} \cite{Foot} \cite{GattringerLang} \cite{Georgi} \cite{GoldsteinSafkoPoole} \cite{GriffithsE} \cite{GriffithsQ} \cite{Grindrod} \cite{HobsonEfstathiouLasenby} \cite{James} \cite{Jeevanjee} \cite{Jones} \cite{Kadanoff} \cite{KibbleBerkshire} \cite{Kleinert} \cite{Leader} \cite{Leader:1} \cite{Leader:2} \cite{LesHouches:93} \cite{Lyons} \cite{Maggiore} \cite{MandlShaw} \cite{Okun} \cite{Paschos} \cite{PercivalRichards} \cite{PeskinSchroeder} \cite{Purdy} \cite{RileyHobson} \cite{RileyHobson:s} \cite{Roberts} \cite{Roberts:A} \cite{Rothe} \cite{SakuraiNapolitano} \cite{Schwartz} \cite{Schweber} \cite{Shankar} \cite{Srednicki} \cite{Stroustrup} \cite{WeinbergGrav} \cite{Weinberg} \cite{Weinberg:1} \cite{Weinberg:2} \cite{Weinberg:3} \cite{WeinbergGrav} \cite{Zee:1} \cite{Zee:2} \cite{ZinnJustin}\\
% Cite sample journal article I took the citation from DiscoverEd
%\cite{HammondRichardT2015Nm}
% Cite samples from latex template
%\cite{seger}

% Print the bibliography
\addcontentsline{toc}{section}{References}
\printbibliography

\end{document}
