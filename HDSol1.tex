\documentclass[12pt,twoside,a4paper]{article}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
%\usepackage[makeroom]{cancel}
\usepackage{cancel}
\usepackage{color}
\usepackage{fancyhdr}
\usepackage[a4paper,left=2cm,right=2cm]{geometry}
\usepackage{graphicx}
\usepackage[colorlinks]{hyperref}
\usepackage{ifthen}
\usepackage[utf8]{inputenc}
\usepackage{lastpage}
\usepackage{physics}
\usepackage{relsize}
\usepackage{simplewick}
\usepackage{subcaption}
\usepackage{tikz}
\usetikzlibrary{patterns}

%Biblatex
%\usepackage[style=numeric,sorting=nyt,isbn=true,backend=biber]{biblatex}
%\addbibresource{HDSol.bib}

% Define some commands I use regularly
\newcommand*{\lr}[1]{\left( {#1} \right)}
\newcommand*{\lrb}[1]{\left[ {#1} \right]}
\newcommand*{\lrc}[1]{\left\{ {#1} \right\}}
\newcommand*{\e}{\operatorname{e}}
\newcommand*{\im}{\operatorname{i}}
\newcommand*{\qop}[1]{\mathrm{\widehat{#1}}}
\newcommand*{\diracdelta}[2][]{\, \delta \ifthenelse{\equal{#1}{}}{}{^{\left({#1}\right)}} \left( {#2} \right)}
\newcommand*{\dx}[1][]{\dd[#1]{x}}
\newcommand*{\pathint}[1][]{\int \mathcal{D}{#1}\,}
\DeclareMathOperator{\cosec}{cosec}
% Blackboard 1 = \mathbbold{1}
\newcommand{\bbfamily}{\fontencoding{U}\fontfamily{bbold}\selectfont}
\DeclareMathAlphabet{\mathbbold}{U}{bbold}{m}{n}

% style allowing styles to be applied to each segment of a path
% http://tex.stackexchange.com/questions/3161/tikz-how-to-draw-an-arrow-in-the-middle-of-the-line
\usetikzlibrary{decorations.pathreplacing,decorations.markings}
\tikzset{
  on each segment/.style={
    decorate,
    decoration={
      show path construction,
      moveto code={},
      lineto code={
        \path [#1]
        (\tikzinputsegmentfirst) -- (\tikzinputsegmentlast);
      },
      curveto code={
        \path [#1] (\tikzinputsegmentfirst)
        .. controls
        (\tikzinputsegmentsupporta) and (\tikzinputsegmentsupportb)
        ..
        (\tikzinputsegmentlast);
      },
      closepath code={
        \path [#1]
        (\tikzinputsegmentfirst) -- (\tikzinputsegmentlast);
      },
    },
  },
  % style to add an arrow in the middle of a path
  mid arrow/.style={postaction={decorate,decoration={
        markings,
        mark=at position .5 with {\arrow[#1]{stealth}}
      }}},
}

%Now define values for my document layout and header
\pagestyle{fancy}
\setlength{\voffset}{-0.5in}
\addtolength{\textheight}{89pt}
\setlength{\headheight}{33pt}
\parindent 0pt
\parskip 12pt
\title{PHYS11012 Hamiltonian Dynamics}
\lfoot{PHYS11012 Hamiltonian Dynamics}
\author{Example Sheet 1 Solutions}
\rfoot{Example Sheet 1 Solutions}
\date{Fri 22$^{\mathrm{nd}}$ Jan 2021}
\cfoot{\thepage{} of \pageref{LastPage}}
\lhead{\includegraphics[scale=0.4]{crest.pdf}}

\begin{document}
\thispagestyle{empty}
\maketitle
\begin{center}
\includegraphics{crest.pdf}\\
\end{center}

% Section headings: Question n
\def\thesection{Q\arabic{section}}
%\def\thesection{\arabic{section}}
\def\thesubsection{\arabic{section}.\alph{subsection}}
\def\thesubsubsection{\arabic{section}.\alph{subsection}.\roman{subsubsection}}

Latest version: \href{https://git.ecdf.ed.ac.uk/s1786208/HD21}{https://git.ecdf.ed.ac.uk/s1786208/HD21}

% Table of contents
\tableofcontents

\newpage
%1
\section{Bead on light rod on horizontal table}

\subsection{Classify the constraints}
As a function of the generalised coordinate, $q$:
\begin{align}
\label{eq:q1ax} x &= q \cos \lr{\omega t}\\
\label{eq:q1ay} y &= q \sin \lr{\omega t}\\
\label{eq:q1az} z &= 0
\intertext{Dividing \eqref{eq:q1ay} by \eqref{eq:q1ax} we have a \emph{holonomic} (independent of velocities), \emph{rheonomous} (time-dependent -- as opposed to \emph{scleronomous}=time independent) constraint:}
0 &= \frac{y}{x} - \tan \lr{\omega t}
\intertext{which, together with \eqref{eq:q1az} makes 2 constraints.}
\intertext{One particle ($N=1$) in 3 dimensions with 2 constraints leaves:}
f &= 3 \times 1 - 2 = 1
\end{align}
degrees of freedom.

\subsection{Solve Lagrange's equations}

The Lagrangian is given by:
\begin{align}
L &= T - V\\
&= \frac{1}{2} m \vec{\dot{r}}\,^2\\
&= \frac{1}{2} m \lrc{\lrb{\dot{q} \cos \lr{\omega t} - \omega q \sin \lr{\omega t}}^2 + \lrb{\dot{q} \sin \lr{\omega t} + \omega q \cos \lr{\omega t}}^2}\\
&= \frac{1}{2} m \big\{ \dot{q}^2 \cos^2 \lr{\omega t} - 2 \dot{q} \cos \lr{\omega t} \omega q \sin \lr{\omega t} +  \omega^2 q^2 \sin^2 \lr{\omega t}\\
&\qquad \quad + \dot{q}^2 \sin^2 \lr{\omega t} + 2 \dot{q} \sin \lr{\omega t} \omega q \cos \lr{\omega t} + \omega^2 q^2 \cos^2 \lr{\omega t} \big\}
\intertext{i.e. our Lagrangian is:}
L &= \frac{1}{2} m \lrb{\dot{q}^2 + \omega^2 q^2}
\intertext{Solving Euler-Lagrange:}
\label{eq:E_L} 0 &= \dv{t} \lr{\pdv{L}{\dot{q}}} - \pdv{L}{q}\\
&= \dv{t} \lr{m \dot{q}} - m \omega^2 q
\intertext{i.e. the equation of motion is:}
\ddot{q} &= \omega^2 q
\intertext{which has solutions:}
q &= A \e^{\omega t} + B \e^{- \omega t}
\end{align}
for constants $A$ and $B$. This is unstable.

\section{Gauge transformation}

Under a gauge transformation, the Lagrangian transforms like so:
\begin{align}
L \longmapsto L' \lr{\vec{q}, \vec{\dot{q}}, t} &= L \lr{\vec{q}, \vec{\dot{q}}, t} + \dv{F \lr{\vec{q}, t}}{t}\\
&= L \lr{\vec{q}, \vec{\dot{q}}, t} + \pdv{F}{t} \dv{t}{t} + \pdv{F}{q_j} \dv{q_j}{t}
\intertext{i.e. if the Lagrangian is invariant up to a total derivative of a function independent of velocities:}
L' \lr{\vec{q}, \vec{\dot{q}}, t} &= L \lr{\vec{q}, \vec{\dot{q}}, t} + \pdv{F}{t} + \pdv{F}{q_j} \dot{q}_j
\intertext{Taking the derivatives we'll need for E-L \eqref{eq:E_L}:}
\pdv{L'}{\dot{q}_i} &= \pdv{L}{\dot{q}_i} + \pdv{\dot{q}_i} \lr{\pdv{F}{t} + \pdv{F}{q_j} \dot{q}_j}\\
&= \pdv{L}{\dot{q}_i} + \pdv{F}{q_i}
\intertext{and:}
\pdv{L'}{q_i} &= \pdv{L}{q_i} + \pdv{q_i} \lr{\pdv{F}{t} + \pdv{F}{q_j} \dot{q}_j}\\
&= \pdv{L}{q_i} + \pdv{F}{q_i}{t} + \pdv{F}{q_i}{q_j} \dot{q}_j
\intertext{E-L \eqref{eq:E_L} gives:}
\dv{t} \lr{\pdv{L'}{\dot{q}_i}} - \pdv{L'}{q_i} &= \dv{t} \lr{\pdv{L}{\dot{q}_i} + {\color{red}\pdv{F}{q_i}}} - \lr{\pdv{L}{q_i} + \pdv{F}{q_i}{t} + \pdv{F}{q_i}{q_j} \dot{q}_j}\\
&= \dv{t} \lr{\pdv{L}{\dot{q}_i}} + {\color{red} \cancel{\pdv{F}{t}{q_i} \dv{t}{t}} + \bcancel{\pdv{F}{q_j}{q_i} \dv{q_j}{t}}} - \pdv{L}{q_i} - \cancel{\pdv{F}{q_i}{t}} - \bcancel{\pdv{F}{q_i}{q_j} \dot{q}_j}
\intertext{i.e.:}
\dv{t} \lr{\pdv{L'}{\dot{q}_i}} - \pdv{L'}{q_i} &= \dv{t} \lr{\pdv{L}{\dot{q}_i}} - \pdv{L}{q_i}
\end{align}
so the gauge transformed Lagrangian obeys the same equations of motion as the original.

\section{System of $N$ particles}

\subsection{Cancelling dots}

A system of $N$ particles in 3 dimensions described by $f$ generalised coordinates is subject to $3 N - f$ (holonomic) constraints. The position of the $k$-th particle is:
\begin{align}
\vec{r}_k &= \vec{r}_k \lr{q_1, q_2, \ldots, q_f, t}
\intertext{By definition:}
\dot{\vec{r}}_k &\equiv \dv{\vec{r}_k}{t}\\
&= \pdv{\vec{r}_k}{t} \dv{t}{t} + \pdv{\vec{r}_k}{q_j} \dv{q_j}{t}
\intertext{i.e.:}
\label{eq:q3a1} \dot{\vec{r}}_k &= \pdv{\vec{r}_k}{t} + \pdv{\vec{r}_k}{q_j} \, \dot{q}_j
\intertext{Taking the partial derivative of both sides with respect to $\dot{q}_i$ shows we can `cancel dots':}
\pdv{\dot{\vec{r}}_k}{\dot{q}_i} &= \pdv{\vec{r}_k}{q_i}
\end{align}

\subsection{Kinetic energy of the system}

The kinetic energy of the system is the sum of the kinetic energies of the particles, i.e.:
\begin{align}
T &= \frac{1}{2} \sum_{k=1}^N m_k \dot{\vec{r}\,}_k^2
\intertext{from \eqref{eq:q3a1}:}
T &= \frac{1}{2} \sum_{k=1}^N m_k \lr{\pdv{\vec{r}_k}{t} + \pdv{\vec{r}_k}{q_j} \, \dot{q}_j}^2\\
&= \frac{1}{2} \sum_{k=1}^N m_k \lr{\lr{\pdv{\vec{r}_k}{t}}^2 + 2 \pdv{\vec{r}_k}{t} \cdot \pdv{\vec{r}_k}{q_j} \, \dot{q}_j + \pdv{\vec{r}_k}{q_i} \cdot \pdv{\vec{r}_k}{q_j} \, \dot{q}_i \dot{q}_j}
\intertext{i.e.:}
\label{eq:q3T} T &= M_0 + \sum_{i = 1}^f M_i \dot{q}_i + \frac{1}{2} \sum_{i, j = 1}^f M_{ij} \dot{q}_i \dot{q}_j
\intertext{where we defined:}
\label{eq:q3M0} M_0 &= \frac{1}{2} \sum_{k = 1}^N m_k \lr{\pdv{\vec{r}_k}{t}}^2\\
\label{eq:q3Mi} M_i &= \sum_{k = 1}^N m_k \pdv{\vec{r}_k}{t} \cdot \pdv{\vec{r}_k}{q_i}\\
\label{eq:q3Mij} M_{ij} &= \sum_{k = 1}^N m_k \pdv{\vec{r}_k}{q_i} \cdot \pdv{\vec{r}_k}{q_j}
\end{align}
and we note that $M_{ij}$ is symmetric under $i \leftrightarrow j$.

\subsection{Scleronomous constraints}

If the constraints are time-independent, then:
\begin{align}
\pdv{\vec{r}_k}{t} &= 0
\intertext{So we see immediately from \eqref{eq:q3M0} and \eqref{eq:q3Mi} that:}
M_0 &= M_i = 0
\intertext{i.e. from \eqref{eq:q3T}:}
\label{eq:q3TNow} T &= \frac{1}{2} \sum_{i, j = 1}^f M_{ij} \dot{q}_i \dot{q}_j
\intertext{Now:}
\pdv{L}{\dot{q}_i} &= \pdv{\dot{q}_i} \lr{T - V}
\intertext{But if the potential is velocity-independent then:}
\pdv{V}{\dot{q}_i} &= 0
\intertext{so:}
\pdv{L}{\dot{q}_i} &= \pdv{T}{\dot{q}_i}
\intertext{and, noting that $M_{ij}$ is symmetric under $i \leftrightarrow j$:}
\pdv{L}{\dot{q}_i} &= \sum_{j = 1}^f M_{ij} \dot{q}_j
\intertext{so:}
\pdv{L}{\dot{q}_i} \dot{q}_i &= \sum_{i, j = 1}^f M_{ij} \dot{q}_i \dot{q}_j
\intertext{i.e. from \eqref{eq:q3TNow}:}
\pdv{L}{\dot{q}_i} \dot{q}_i &= 2 T
\end{align}
\emph{Aside}: Skipping forward slightly, we have shown that in this case, the Legendre Transform, $H = p_i \dot{q}_i - L$, gives us the Hamiltonian, $H = 2T - \lr{T - V} = T + V$.

\section{Charged particle in electromagnetic field}

\subsection{Background}

We need some vector calculus identities, e.g. the div of a curl always vanishes, i.e. for any $\vec{z}$:
\begin{align}
\label{eq:vec_div_curl} \div \lr{\curl \vec{z}\,} &= 0
\intertext{Similarly, the curl of a grad always vanishes, i.e. for any scalar $z$:}
\label{eq:vec_curl_grad} \curl \lr{ \grad z} &= 0
\end{align}

\subsubsection{Maxwell's equations}

The two Maxwell equations we need are:
\begin{align}
\label{eq:Max1} \div \vec{B} &= 0\\
\label{eq:Max2} \curl \vec{E} + \frac{1}{c} \pdv{\vec{B}}{t} &= 0
\end{align}

\subsubsection{A useful identity}

We will need to either know, or derive, this identity. Starting with:
\begin{align}
\lr{\vec{a} \times \vec{b}}_i &= \epsilon_{ijk} a_j b_k
\intertext{we evaluate the identity we will need:}
\lrb{\dot{\vec{r}\,} \times \lr{\grad \times \vec{A}}}_i &= \epsilon_{ijk} \dot{r}_j \epsilon_{klm} \partial_l A_m\\
&= \lr{\delta_{il} \delta_{jm} - \delta_{im} \delta_{jl}} \dot{r}_j \partial_l A_m\\
&= \dot{r}_j \partial_i A_j - \dot{r}_j \partial_j A_i\\
&= \dot{\vec{r}\,} \cdot \pdv{\vec{A}}{r_i} - \dot{\vec{r}\,} \cdot \grad A_i
\intertext{which we rearrange to get in the form we'll need:}
\dot{\vec{r}\,} \cdot \pdv{\vec{A}}{r_i} &= \lrb{\dot{\vec{r}\,} \times \lr{\grad \times \vec{A}}}_i + \dot{\vec{r}\,} \cdot \grad A_i
\intertext{Or, using the notation:}
\pdv{\vec{r}_i} &\equiv \sum_i \pdv{r_i}
\intertext{Our useful identity becomes:}
\label{eq:q4I} \dot{\vec{r}\,} \cdot \pdv{\vec{A}}{\vec{r}} &= \dot{\vec{r}\,} \times \lr{\grad \times \vec{A}} + \dot{\vec{r}\,} \cdot \grad \vec{A}
\end{align}

\subsubsection{Maxwell's equations}

\subsection{Part 1: The Lagrangian describes the motion}

We are given the Lagrangian:
\begin{align}
\label{eq:q4L} L \lr{\vec{r}, \dot{\vec{r}\,}} &= \frac{1}{2} m \dot{\vec{r}\,}^2 - \lr{-e} \lr{\phi \lr{\vec{r}\,} - \frac{\dot{\vec{r}}}{c} \cdot \vec{A} \lr{\vec{r}\,}}
\intertext{Which depends as usual on the particle's velocity via the kinetic term, and on the particle's position via the potential:}
\phi \lr{\vec{r}\,} &\equiv \textrm{Scalar potential}\\
\vec{A} \lr{\vec{r}\,} &\equiv \textrm{Vector potential}
\intertext{Per the hint, we show that the equation of motion arising from $L$ is the Lorentz force law:}
\label{eq:Lorentz} m \ddot{\vec{r}} &= -e \lrb{\vec{E} + \frac{\dot{\vec{r}}}{c} \times \vec{B}}
\intertext{and we do this in the usual way by solving E-L:}
\label{eq:q4E_L} 0 &= \dv{t} \lr{\pdv{L}{\dot{\vec{r}\,}}} - \pdv{L}{\vec{r}\,}
\intertext{Taking the appropriate derivatives of \eqref{eq:q4L}:}
\dv{t} \lr{\pdv{L}{\dot{\vec{r}\,}}} &= \dv{t} \lr{m \dot{\vec{r}\,} - \frac{e}{c} \vec{A}}\\
&= m \ddot{\vec{r}\,} - \frac{e}{c} \dv{\vec{A}}{t}\\
\label{eq:q4a1} &= m \ddot{\vec{r}\,} - \frac{e}{c} \lr{\pdv{\vec{A}}{t} + \dot{\vec{r}\,} \cdot \grad \vec{A}}
\intertext{and:}
\pdv{L}{\vec{r}\,} &= e \grad \phi - \frac{e}{c} \dot{\vec{r}\,} \cdot \pdv{\vec{A}}{\vec{r}}
\intertext{and we apply our handy identity \eqref{eq:q4I} to the second term to obtain:}
\label{eq:q4a2} \pdv{L}{\vec{r}\,} &= e \grad \phi - \frac{e}{c} \lr{\dot{\vec{r}\,} \times \lr{\grad \times \vec{A}} + \dot{\vec{r}\,} \cdot \grad \vec{A}}
\intertext{Inserting \eqref{eq:q4a1} and \eqref{eq:q4a2} into \eqref{eq:q4E_L}, we see that the last two terms in \eqref{eq:q4a1} and \eqref{eq:q4a2} cancel:}
0 &= m \ddot{\vec{r}\,} - \frac{e}{c} \pdv{\vec{A}}{t} - e \grad \phi + \frac{e}{c} \dot{\vec{r}\,} \times \lr{\grad \times \vec{A}}\\
\label{eq:q4a3} m \ddot{\vec{r}\,} &= e \lrb{\frac{1}{c} \pdv{\vec{A}}{t} + \grad \phi - \frac{\dot{\vec{r}\,}}{c} \times \lr{\grad \times \vec{A}}}
\intertext{which is almost in the form we need, so we just need to manipulate Maxwell's equations.}
\intertext{Now, because \eqref{eq:vec_div_curl} a div of a curl always vanishes, \eqref{eq:Max1} is automatically satisfied by defining:}
\label{eq:q4a4} \vec{B} &= \curl \vec{A}
\intertext{Substituting this into \eqref{eq:Max2}:}
0 &= \curl \vec{E} + \frac{1}{c} \pdv{t} \lr{\curl \vec{A}}\\
\label{eq:q4a5} 0 &= \curl \lr{\vec{E} + \frac{1}{c} \pdv{\vec{A}}{t}}
\intertext{and because \eqref{eq:vec_curl_grad} the curl of a grad always vanishes, \eqref{eq:q4a5} is satisfied by defining $\phi$ such that:}
- \grad \phi &= \vec{E} + \frac{1}{c} \pdv{\vec{A}}{t}\\
\label{eq:q4a6} - \vec{E} &= \frac{1}{c} \pdv{\vec{A}}{t} + \grad \phi
\intertext{Inserting \eqref{eq:q4a4} and \eqref{eq:q4a6} into \eqref{eq:q4a3} we now have the eom in the form we were looking for \eqref{eq:Lorentz}:}
m \ddot{\vec{r}} &= -e \lrb{\vec{E} + \frac{\dot{\vec{r}}}{c} \times \vec{B}}
\end{align}
so this Lagrangian does describe the charged particle.

\subsection{Part 2: Angular velocity for symmetric potentials}

We use cylindrical polar coordinates $\lr{r, \theta, z}$ to describe scalar and vector potentials which are symmetric about the $z$-axis, i.e. independent of $\theta$ (which is an \emph{ignorable coordinate}):
\begin{align}
\phi &= \phi \lr{r, z}\\
\vec{A} &= \lr{A_r \lr{r, z}, \frac{f \lr{r}}{r}, A_z \lr{r, z}}
\intertext{in cylindrical polar coordinates (ignoring $\theta$ because, as we noted, it is an ignorable coordinate):}
\vec{r} &= r \vec{e}_r + z \vec{e}_z\\
\dot{\vec{r}\,} &= \dot{r} \vec{e}_r + r \dot{\theta} \vec{e}_\theta + \dot{z} \vec{e}_z
\intertext{Our Lagrangian \eqref{eq:q4L} thus becomes:}
\label{eq:q4b1} L \lr{\vec{r}, \dot{\vec{r}\,}} &= \frac{1}{2} m \lr{\dot{r}^2 + r^2 \dot{\theta}^2 + \dot{z}^2} + e \lr{\phi - \frac{1}{c} \lr{\dot{r} A_r + \dot{\theta} f + \dot{z} A_z}}
\intertext{and we note that $\theta$ being an ignorable coordinate led to \eqref{eq:q4b1} being independent of $\theta$. When given a Lagrangian, our reasoning is reversed: $\pdv*{L}{q_i} = 0 \implies q_i$ is an ignorable coordinate.}
\intertext{For ignorable coordinates in general, and $\theta$ in this case, E-L \eqref{eq:E_L} gives:}
\pdv{L}{\dot{\theta}} &= k
\intertext{where $k$ is a constant. We find $\pdv*{L}{\dot{\theta}}$:}
\pdv{L}{\dot{\theta}} &= m r^2 \dot{\theta} - \frac{e}{c} \, f
\intertext{We are given boundary conditions $\theta = \dot{\theta} = 0$ at $t = 0$, so defining $r_0$ as the initial radius:}
k &= - \frac{e}{c} f \lr{r_0}
\intertext{so:}
m r^2 \dot{\theta} &= \frac{e}{c} \lrb{f \lr{r} - f \lr{r_0}}
\intertext{i.e. the angular velocity about the $z$-axis is the expression we were asked to find:}
\dot{\theta} &= \frac{e}{m c r^2} \lrb{f \lr{r} - f \lr{r_0}}
\end{align}

\section{}

\subsection{System of $N$ particles}

The centre of mass/momentum frame is the inertial frame such that the instantaneous total momentum of the system is zero, i.e.:
\begin{align}
\label{eq:q5a1} 0 &= \vec{p}^{\,\lr{\textrm{cm}}} = \sum_{i = 1}^N \vec{p}^{\,\lr{\textrm{cm}}}_i = \sum_{i = 1}^N m_i \vec{v}^{\,\lr{\textrm{cm}}}_i
\intertext{For an arbitrary inertial frame, $S$, defining $\vec{v}$ as the velocity of the centre of momentum frame with respect to $S$, we have:}
\vec{v}_i &= \vec{v}^{\,\lr{\textrm{cm}}}_i + \vec{v}
\intertext{The kinetic energy in $S$ is thus:}
T &= \frac{1}{2} \sum_{i = 1}^N m_i \, \vec{v}_i^{\, 2}\\
&= \frac{1}{2} \sum_{i = 1}^N m_i \lr{\vec{v}_i^{\,\lr{\textrm{cm}}} + \vec{v}}^2\\
&= \frac{1}{2} \sum_{i = 1}^N m_i \vec{v}_i^{\,\lr{\textrm{cm}} \, 2} + \lr{\sum_{i = 1}^N m_i \vec{v}_i^{\,\lr{\textrm{cm}}}} \cdot \vec{v} + \frac{1}{2} \sum_{i = 1}^N m_i \vec{v}^{\, 2}\\
&= \frac{1}{2} \sum_{i = 1}^N m_i \vec{v}_i^{\,\lr{\textrm{cm}} \, 2} + \frac{1}{2} \sum_{i = 1}^N m_i \vec{v}^{\, 2}
\intertext{where the cross-term vanishes because of \eqref{eq:q5a1}, i.e.:}
T &= T^{\,\lr{\textrm{cm}}} + \frac{1}{2} M \vec{v}^{\, 2}
\intertext{with:}
T^{\,\lr{\textrm{cm}}} &= \frac{1}{2} \sum_{i = 1}^N m_i \vec{v}_i^{\,\lr{\textrm{cm}} \, 2}\\
M &= \sum_{i = 1}^N m_i\\
\end{align}

\subsection{One end of rod sliding along wire}

We will use $x$ and $\theta$ as generalised coordinates, per the diagram:
\begin{figure}[ht]
\begin{center}
\begin{tikzpicture}[scale=1.35]
% Axes
\draw[ultra thin, gray, ->, dashed] (-2,0) -- (5,0);
\draw[ultra thin, gray, ->, dashed] (0,-3.2) -- (0,0.5) node[black, left]{$y$};
\draw[ultra thin, gray, ->, dashed] (6,-1.5) -- (6,-0.5) node[black, left]{$\vec{e}_y$};
\draw[ultra thin, gray, ->, dashed] (6,-1.5) -- (6.8,-1.5) node[black, right]{$\vec{e}_x$};
% Wire
\draw[thick, red] (-1,0) -- (4,0) node[above]{wire};
% Rod
\draw[ultra thick, blue] (1,0) -- (3,-3) node[right]{rod};
\filldraw[blue] (2,-1.5) circle(0.07) node[below left]{CofM};
% Working
\draw[ultra thin, gray, dashed] (1,0) -- (1,-1.5) -- (2, -1.5) -- (2,0);
\draw[ultra thin, gray, dashed] (1.1,-1.5) -- (1.1,-1.4) -- (1, -1.4);
\draw[ultra thin, gray, dashed] (1.9,0) -- (1.9,-0.1) -- (2, -0.1);
% Angles
\draw[gray,->,>=stealth,semithick] ([shift=(-90:0.5)]1,0) arc (-90:-55:0.5);
\node[black] at (1.2,-0.72) {$\theta$};
\draw[gray,->,>=stealth,semithick] ([shift=(90:0.5)]2,-1.5) arc (90:125:0.5);
\node[black] at (1.8,-0.78) {$\theta$};
% L/2 on rod
\node[rotate=-56,black,blue] at (1.5,-0.4) {$\flatfrac{L}{2}$};
\node[rotate=-56,black,blue] at (2.7,-2.25) {$\flatfrac{L}{2}$};
% Labels
\draw[ultra thin, ->, dashed] (2.13,0) -- (2.13,-0.75) node {$h$} -- (2.13,-1.5);
\draw[ultra thin, ->, dashed] (0,0.13) -- (0.5,0.13) node {$x$} -- (1,0.13);
\end{tikzpicture}
\caption{Rod with one end sliding along wire}
\label{fig:Q5_Fig1}
\end{center}
\end{figure}

The potential of the rod, $V$, for some constant, $\kappa$, is:
\begin{align}
V &= -M g h + \kappa\\
&= - M g \frac{L}{2} \cos \theta + \kappa
\intertext{The kinetic energy of the rod is the sum of translational and rotational energy:}
\label{eq:q5b1} T &= T^{\textrm{translate}} + T^{\textrm{rotate}}
\intertext{where, because we are given $I = \flatfrac{M L^2}{12}$:}
T^{\textrm{rotate}} &\equiv \frac{1}{2} I \dot{\theta}^2\\
\label{eq:q5b2} &= \frac{1}{24} M L^2 \dot{\theta}^2
\intertext{and:}
T^{\textrm{translate}} &= \frac{1}{2} M \vec{v}^{\, 2}
\intertext{where $\vec{v}$ is the velocity of the centre of mass:}
x^{\lr{\textrm{cm}}} &= x + \frac{L}{2} \sin \theta\\
y^{\lr{\textrm{cm}}} &t= - \frac{L}{2} \cos \theta
\intertext{so:}
v &= \dot{x}^{\lr{\textrm{cm}}} \vec{e}_x + \dot{y}^{\lr{\textrm{cm}}} \vec{e}_y\\
&= \lr{\dot{x} + \frac{L}{2} \dot{\theta} \cos \theta} \vec{e}_x + \frac{L}{2} \dot{\theta} \sin \theta \, \vec{e}_y\\
\intertext{i.e.:}
T^{\textrm{translate}} &= \frac{1}{2} M \lrb{\lr{\dot{x} + \frac{L}{2} \dot{\theta} \cos \theta}^2 + \lr{\frac{L}{2} \dot{\theta} \sin \theta}^2}\\
\label{eq:q5b3} &= \frac{1}{2} M \lr{\dot{x}^2 + \frac{1}{4} L^2 \dot{\theta}^2 + L \dot{x} \dot{\theta} \cos \theta}
\intertext{Inserting \eqref{eq:q5b3} and \eqref{eq:q5b2} into \eqref{eq:q5b1}:}
T &= \frac{1}{2} M \lr{\dot{x}^2 + \frac{1}{3} L^2 \dot{\theta}^2 + L \dot{x} \dot{\theta} \cos \theta}
\intertext{The Lagrangian, $\mathcal{L}$, is (exercising our freedom to choose $\kappa = 0$):}
\mathcal{L} &= T - V\\
&= \frac{1}{2} M \lr{\dot{x}^2 + \frac{1}{3} L^2 \dot{\theta}^2 + L \dot{x} \dot{\theta} \cos \theta} + \frac{1}{2} M g L \cos \theta
\end{align}
which is what we were asked to find.\\
\hrule

$\mathcal{L}$ is independent of time, so there is another constant, $h$:
\begin{align}
h &= \pdv{\mathcal{L}}{\dot{q}_i} \dot{q}_i - \mathcal{L}
\intertext{Moreover, since $T$ is quadratic in the velocities, $E \equiv h$:}
\label{eq:q5bE} E &= T + V
\end{align}
\hrule

The boundary conditions given are:
\begin{align}
\theta \lr{0} &= 0 & \dot{\theta} \lr{0} &= \omega\\
x \lr{0} &= 0 & \dot{x} \lr{0} &= 0
\end{align}
$\mathcal{L}$ is also independent of $x$, so $p_x$ is another constant of the motion:
\begin{align}
p_x &= \pdv{\mathcal{L}}{\dot{x}}\\
&= M \dot{x} + \frac{1}{2} M L \dot{\theta} \cos \theta
\intertext{Using the boundary conditions on the left hand side:}
\frac{1}{2} M L \omega &= M \dot{x} + \frac{1}{2} M L \dot{\theta} \cos \theta
\intertext{We rearrange to get $\dot{x}$ in terms of $\theta$ and $\dot{\theta}$ only:}
\dot{x} &= \frac{1}{2} L \lr{\omega - \dot{\theta} \cos \theta}
\intertext{and use this to get E in terms of $\theta$ and $\dot{\theta}$ only:}
E &= \frac{1}{2} M \lr{\frac{1}{4} L^2 \lr{\omega - \dot{\theta} \cos \theta}^2 + \frac{1}{3} L^2 \dot{\theta}^2 + \frac{1}{2} L^2 \lr{\omega - \dot{\theta} \cos \theta} \dot{\theta} \cos \theta} - \frac{1}{2} M g L \cos \theta\\
&= \frac{1}{2} M L^2 \lr{\frac{1}{2} \lr{\omega - \dot{\theta} \cos \theta} \lrb{\frac{1}{2} \lr{\omega - \dot{\theta} \cos \theta} + \dot{\theta} \cos \theta} + \frac{1}{3} \dot{\theta}^2} - \frac{1}{2} M g L \cos \theta\\
&= \frac{1}{2} M L^2 \lr{\frac{1}{2} \lr{\omega - \dot{\theta} \cos \theta} \frac{1}{2} \lr{\omega + \dot{\theta} \cos \theta} + \frac{1}{3} \dot{\theta}^2} - \frac{1}{2} M g L \cos \theta
\intertext{i.e. the general expression for the (constant) energy is:}
E &= \frac{1}{2} M L^2 \lr{\frac{1}{4} \lr{\omega^2 - \dot{\theta}^2 \cos^2 \theta} + \frac{1}{3} \dot{\theta}^2} - \frac{1}{2} M g L \cos \theta
\intertext{The boundary conditions tell us that:}
E &= \frac{1}{6} M L^2 \omega^2 - \frac{1}{2} M g L
\intertext{But the rod comes briefly to rest at $\theta = \theta_m$, so we have another expression for $E$:}
E &= \frac{1}{8} M L^2 \omega^2 - \frac{1}{2} M g L \cos \theta_m
\end{align}
Equating these two terms:
\begin{align}
\frac{1}{8} L \omega^2 - \frac{1}{2} g \cos \theta_m &= \frac{1}{6} L \omega^2 - \frac{1}{2} g\\
\frac{1}{2} g \lr{1 - \cos \theta_m} &= \omega^2 L \lr{\frac{1}{6} - \frac{1}{8}}\\
\sin^2 \lr{\frac{\theta_m}{2}} &= \frac{\omega^2 L}{24 g}
\end{align}
which is the expression given for the maximum angle the rod rises.

% Cite my reference library (test)
%\cite{AitchisonHey} \cite{AitchisonHey:1} \cite{AitchisonHey:2} \cite{ArfkenWeberHarris} \cite{Bauer} \cite{Becker} \cite{BjorkenDrell} \cite{BurgessMoore} \cite {ByronFuller} \cite{Cardy} \cite{ChaikinLubensky} \cite{ChengLi} \cite{ChengLiProbs} \cite{CottinghamGreenwood} \cite{Efron} \cite{EllisStirlingWebbers} \cite{FeynmanHibbs} \cite{Foot} \cite{GattringerLang} \cite{Georgi} \cite{GoldsteinSafkoPoole} \cite{GriffithsE} \cite{GriffithsQ} \cite{Grindrod} \cite{HobsonEfstathiouLasenby} \cite{James} \cite{Jeevanjee} \cite{Jones} \cite{Kadanoff} \cite{KibbleBerkshire} \cite{Kleinert} \cite{Leader} \cite{Leader:1} \cite{Leader:2} \cite{LesHouches:93} \cite{Lyons} \cite{Maggiore} \cite{MandlShaw} \cite{Okun} \cite{Paschos} \cite{PercivalRichards} \cite{PeskinSchroeder} \cite{Purdy} \cite{RileyHobson} \cite{RileyHobson:s} \cite{Roberts} \cite{Roberts:A} \cite{Rothe} \cite{SakuraiNapolitano} \cite{Schwartz} \cite{Schweber} \cite{Shankar} \cite{Srednicki} \cite{Stroustrup} \cite{WeinbergGrav} \cite{Weinberg} \cite{Weinberg:1} \cite{Weinberg:2} \cite{Weinberg:3} \cite{WeinbergGrav} \cite{Zee:1} \cite{Zee:2} \cite{ZinnJustin}\\
% Cite sample journal article I took the citation from DiscoverEd
%\cite{HammondRichardT2015Nm}
% Cite samples from latex template
%\cite{seger}

% Print the bibliography
%\addcontentsline{toc}{section}{References}
%\printbibliography

\end{document}
